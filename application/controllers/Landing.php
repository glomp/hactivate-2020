<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	// var $domain_name = ;
	var $domain_names = array(
		'localhost' => '', //change this when you like to test
		'192.168.0.25' => 'bodyshop', //change this when you like to test
		'staging2.glomp.it' => 'demo',
		'cordonbleu-promo.com' => 'martell',
		'www.cordonbleu-promo.com' => 'martell',
		'promo-sg.isseymiyakeparfums.com' => 'issey-miyake',
		'www.promo-sg.isseymiyakeparfums.com' => 'issey-miyake',
		'promo-hk.isseymiyakeparfums.com' => 'issey-miyake-hk',
		'www.promo-hk.isseymiyakeparfums.com' => 'issey-miyake-hk',
		//bodshop
		'www.duckndive.info' => 'bodyshop',
		'duckndive.info' => 'bodyshop',
		'www.truthdare.info' => 'bodyshop-b',
		'truthdare.info' => 'bodyshop-b',
		//end bodyshop
		//carlsberg
		'www.beerwithfood.me' => 'carlsberg',
		'beerwithfood.me' => 'carlsberg',
		//end carlsberg
		//shu uemura
		'www.event.hkshugirls.com' => 'uemura',
		'event.hkshugirls.com' => 'uemura',
		//end shu uemura
		//prince - cartier
		'www.princecampaign.dtwdigital.com.hk' => 'cartier',
		'princecampaign.dtwdigital.com.hk' => 'cartier',
		//end prince - cartier
	);

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if ( ! isset($this->domain_names[$_SERVER['HTTP_HOST']])) {
			$campaign = '';
		} else {
			$campaign = $this->domain_names[$_SERVER['HTTP_HOST']];
			if ( ! empty($campaign)) {

				$segment = $this->uri->segment_array(); //refactor for future campaigns

				if (isset($segment[1])) {
					return redirect(base_url('landing/page/' . $campaign. '?channel='. $segment[1]));
				}

				return redirect(base_url('landing/page/' . $campaign));
			}	
		}

		

		$this->load->view('header');  
		$this->load->view('landing/index'); 
        $this->load->view('footer');
	}
    
    public function form()
    {
        $this->load->view('header');
		$this->load->view('landing/form');
        $this->load->view('footer');
    }

    public function page($campaign, $page = 'index', $action = '', $channel = '', $customer_id = 0)
    {
    	$data['action'] = $action;
    	$data['customer_id'] = $customer_id;
    	$url_array = explode("/", uri_string()); 
		
		$this->load->view('landing/'.$campaign.'/header');

		if (array_key_exists (2, $url_array)) {
			$this->load->view('landing/' . $campaign .'/'. $page, $data);
		}  
 
        $this->load->view('footer');
    }


    public function campaign_log()
    {
    	$campaign_id = 1; // hard coded for martell campaign

    	$action = $_POST['action'];
    	$page = $_POST['page'];

    	//check action visit on log field json search using like clause
    	if ($action == 'visitor') {
    		$this->db->like('logs', '"action":"'. $action.'"');
    		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id, 'uid' => $_POST['visit_id']));
    		
    		//if there is no record check latest uid
    		if ($res->num_rows() == 0 ) {
    			$this->db->like('logs', '"action":"'. $action.'"');
    			$this->db->order_by('uid', 'DESC');
    			$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id), 1);

    			$log = array(
					'action' => 'visitor',
					'page' => $page,
				);

    			//if no record create first record
    			if ($res->num_rows() == 0 ) {
    				$data = array(
		                'campaign_id' => $campaign_id,
		                'uid' => 1,
		                'logs' => json_encode($log),
		                'date_created' => date("Y-m-d H:i:s"),
		            );

		            $this->db->insert('h_campaign_log', $data);

		            echo json_encode(array('visit_id' => 1));
		           	return;
    			} else {
    				//get latest id then add new one
    				$data = array(
		                'campaign_id' => $campaign_id,
		                'uid' => $res->row()->uid + 1,
		                'logs' => json_encode($log),
		                'date_created' => date("Y-m-d H:i:s"),
		            );

		            $this->db->insert('h_campaign_log', $data);

		            echo json_encode(array('visit_id' => $data['uid']));
		           	return;
    			}
    		} else {
    			//do nothing
    			echo json_encode(array('visit_id' => 0));
	           	return;
    		}
    	}


    	//check action visit on log field json search using like clause
    	if ($action == 'download') {
    		$channel = $_POST['channel'];
    		
    		$this->db->like('logs', '"action":"'. $action.'"');
    		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id, 'uid' => $_POST['visit_id']));
    		
    		//if there is no record create log
    		if ($res->num_rows() == 0 ) {
    			$log = array(
					'action' => $action,
					'page' => $page,
				);

				$date_created = date("Y-m-d H:i:s");

				//create customer record
				$data = array(
		        	'client_id' => '1', // replace with get client id 
		        	'date_created' => $date_created,
		        	'date_updated' => $date_created
		        );

				$this->db->insert('h_client_customers', $data);
				$uid = $this->db->insert_id();

				//create campaign voucher
				$data = array(
		        	'customer_id' => $uid, 
		        	'campaign_items_id' => 1, //from campaign item table 
		        	'item_id' => 1, //from items table 
		        	'type' => 'consumable',  
		        	'channel' => 'ios',  
		        	'date_created' => $date_created,
		        	'date_updated' => $date_created
		        );

				$this->db->insert('h_campaign_vouchers', $data);
				$campaign_voucher_id = $this->db->insert_id();

				//create campaign voucher history
				$data = array(
		        	'campaign_vouchers_id' => $campaign_voucher_id, 
		        	'outlets_id' => 1, 
		        	'status' => 'unused',
		        	'date_created' => $date_created,
		        );

				$this->db->insert('h_campaign_vouchers_history', $data);

				//create log
				$data = array(
	                'campaign_id' => $campaign_id,
	                'uid' => $uid ,
	                'logs' => json_encode($log),
	                'date_created' => $date_created,
	            );

	            $this->db->insert('h_campaign_log', $data);

	            echo json_encode(array('visit_id' => $uid));
	           	return;
    		} else {
    			//do nothing
    			echo json_encode(array('visit_id' => 0));
	           	return;
    		}
    	}
    	
    }

    public function redeem() {
    	$date_created = date("Y-m-d H:i:s");
    	$campaign_id = 1; // hard coded for martell campaign

    	$action = $_POST['action'];
    	$page = $_POST['page'];
    	$customer_id = $_POST['visit_id'];
    	$log = array(
			'action' => $action,
			'page' => $page,
		);

    	$res = $this->db->get_where('h_campaign_vouchers', array('customer_id' => $customer_id, 'campaign_items_id' => 1));
    	$campaign_voucher_id = $res->row()->id;

    	$res2 = $this->db->get_where('h_campaign_vouchers_history', array('campaign_vouchers_id' => $campaign_voucher_id, 'status' => 'consumed'));

    	//If already redeemed
    	if ($res2->num_rows() > 0) {
    		echo json_encode(array('success' => FALSE));
    		return;
    	}

    	//create campaign voucher history
		$data = array(
        	'campaign_vouchers_id' => $campaign_voucher_id, 
        	'outlets_id' => 1, 
        	'status' => 'consumed',
        	'date_created' => $date_created,
        );

		$this->db->insert('h_campaign_vouchers_history', $data);

		//create log
		$data = array(
            'campaign_id' => $campaign_id,
            'uid' => $customer_id ,
            'logs' => json_encode($log),
            'date_created' => $date_created,
        );

        $this->db->insert('h_campaign_log', $data);

        echo json_encode(array('success' => TRUE));
		return;

    } 



}
