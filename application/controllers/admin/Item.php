<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {


	public function index()
	{
		$data['items'] = array();
		$query = $this->db->get('h_items');
	   
	   if($query->num_rows() > 0)
	   {
	       $data['items'] = $query->result();
	   }

		$this->load->view('admin/item/list',  $data);
	}


	public function add()
	{
		/*
			TODO:
			1. validation
		*/

		if (! empty($_POST) ) {
			$this->load->library('form_validation'); //load form validation class

			$data = $_POST;
			$return_data = array(
				'success' => false,
				'messages' => array()
			);	


			$this->form_validation->set_rules('client_id', 'Client', 'required');
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('image', 'Image', 'required');
			$this->form_validation->set_rules('status', 'Status', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');

			//error message goes here
			if ($this->form_validation->run() == FALSE)
            {
            	//set messages array of validation messages
            	$return_data['messages'] = $this->form_validation->error_array();

            	echo json_encode($return_data);

            	return;

            } else {
            	$this->load->helper('common'); // load helper

          
            	$data['client_id'] = $this->input->post('client_id');
				$data['name'] = $this->input->post('name');
				$data['image'] = $this->input->post('image');
				$data['name'] = $this->input->post('status');
				$data['description'] = $this->input->post('description');
				$data['date_created'] = date('Y-m-d H:i:s');
				$data['date_updated'] = date('Y-m-d H:i:s');

				//print_r($data);

				//saving to database
				$this->db->insert('h_items', $data);
				//set response as success = true
				$return_data['success'] = true;
            }	
			
			
			echo json_encode($return_data);
			return; 
		}
		


		$this->load->view('admin/item/add_items');
	}



}
