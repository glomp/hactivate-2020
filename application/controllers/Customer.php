<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();  
        $this->load->helper('download');
        
        $this->load->model('Customer_Model', '', TRUE);
        $this->load->model('IosVoucher_Model', '', TRUE);
    } 
    
    public function saveCustomer()
    { 
        $success = $this->Customer_Model->saveCustomer($this->input->post());
        $json_data["success"] = 0;
        $json_data["download"] = "";
        
        if ($success === false) {
            
        } else {
            $json_data["success"] = 1;
            $json_data["download"] = base_url("IosVoucher/createVoucher/1/" . $success);
        }
        
        echo json_encode($json_data);
    }
}

?>