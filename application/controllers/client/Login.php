<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index()
    {
        $is_log = $this->session->userdata('logged_in');

        if ($is_log != NULL && $is_log == TRUE) {
            //campaign controller redirect
            $data['dashboard_url'] = 'client/'. $this->session->userdata('campaign_controller') . '/dashboard';
            return redirect(base_url($data['dashboard_url']));
        }

        $this->load->view('client/login');
    }

    public function logout() {

        $array_items = array('username', 'logged_in', 'campaign_controller');

        $this->session->unset_userdata($array_items);

        return redirect(base_url('client/login'));


    }

    public function in() {
        $this->load->library('form_validation'); //load form validation class
        $this->load->helper('common'); //load form validation class

        $return_data = array(
            'success' => false,
            'messages' => array()
        );      
        
        $user_name = $this->input->post('username');
        $user_password = $this->input->post('user_password');
        
        
        $this->form_validation->set_rules('username', 'Name', 'trim|required');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required');

        if ($this->form_validation->run() == TRUE)
        {
            $name = $this->input->post('name');
            $password = $this->input->post('password');
            
            $isValid = false;
            $logged_key = 0; //use to determine what array key is used to logged in

            // $user = $this->db->get_where('h_clients', array(
            //     'name' => $name,
            //     'password' => $password
            // ));

            // $total = $user->num_rows();


            // if ($total > 0) {
            //     $isValid = true;
            // }



            // $query = $this->db->query('SELECT name, password FROM h_clients WHERE name = "'. $name .'" AND password = "'. $password.'"');
            
            // // Add Users & Password here
            $userArray = array(
                array(
                    'username' => 'glompmaster@glomp.it',
                    'password' => '@Glomp123',
                    'campaign' => 'none'
                ),
                array(
                    'username' => 'toby@glomp.it',
                    'password' => '@Glomp123',
                    'campaign' => 'none'
                ),
                array(
                    'username' => 'martell@admin.com',
                    'password' => 'mart3ll@dmin',
                    'campaign' => 'martell'
                ),
                array(
                    'username' => 'issey_miyake@admin.com',
                    'password' => 'isseymiyak3@dmin',
                    'campaign' => 'issey'
                ),
                array(
                    'username' => 'demo@admin.com',
                    'password' => 'demo',
                    'campaign' => 'demo'
                ),
                array(
                    'username' => 'bodyshop@admin.com',
                    'password' => 'b0dysh0p123',
                    'campaign' => 'bodyshop'
                ),
                array(
                    'username' => 'bodyshop2@admin.com',
                    'password' => 'b0dysh0p123',
                    'campaign' => 'bodyshop-b'
                ),
                array(
                    'username' => 'carlsberg@admin.com',
                    'password' => 'carlsb3rg123',
                    'campaign' => 'carlsberg'
                ),
                array(
                    'username' => 'uemura@admin.com',
                    'password' => 'u3mura123',
                    'campaign' => 'uemura'
                ),
                array(
                    'username' => 'prince@admin.com',
                    'password' => 'pr1nc3123',
                    'campaign' => 'cartier'
                )


            );

            foreach($userArray as $key => $user){
                if($user['username'] == $user_name && $user['password'] == $user_password){
                    $isValid = true;
                    $logged_key = $key;
                    break;
                }
            }
           
            if ($isValid)
            {
                
                $return_data['success'] = true;
                $sess_data = array(
                    'username'  => $userArray[$logged_key]['username'],//$user->row()->name,
                    'campaign_controller'  => $userArray[$logged_key]['campaign'],//$user->row()->campaign,
                    'logged_in' => TRUE
                );

                $this->session->set_userdata($sess_data);
            } 
            else
            {
                //User name / Passsword incorrect
                $return_data['messages'] = array('user_password' => 'Incorrect Username / Password');
            }
        } else
        {
            //Validation error
            $return_data['messages'] = $this->form_validation->error_array();
        }

        echo json_encode($return_data);
        return;
    }

}