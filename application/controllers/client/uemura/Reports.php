<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {
	var $campaign_id = 6;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function page_visitor() 
	{
		$data['sidebar_view'] = 'client/uemura/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_page_visitors());
			return;
		}

		$this->load->view('client/uemura/reports/page_visitors', $data);
	}

	public function registrants() 
	{
		$data['sidebar_view'] = 'client/uemura/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_registrants());
			return;
		}

		$this->load->view('client/uemura/reports/registrants', $data);
	}

	public function download_from_android() 
	{
		$data['sidebar_view'] = 'client/uemura/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_download_from_android());
			return;
		}

		$this->load->view('client/uemura/reports/download_from_android', $data);
	}

	public function download_from_ios() 
	{

		$data['sidebar_view'] = 'client/uemura/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_download_from_ios());
			return;
		}

		$this->load->view('client/uemura/reports/download_from_ios', $data);
	}

	public function redemptions($channel = 'ios') 
	{
		$data['sidebar_view'] = 'client/uemura/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_redemptions($channel));
			return;
		}


		$this->load->view('client/uemura/reports/redemptions', $data);
	}

	public function export_registrants() {
		$campaign_id = $this->campaign_id;

		//data with offset limit
		$this->db->select('id, uid, date_created');
		$this->db->like('logs', '"action":"register","page":"register-popup"');
		$this->db->order_by('date_created DESC');
		//$this->db->limit(10);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		$this->load->library('excel');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("h.@ctiv8")
									 ->setLastModifiedBy("h.@ctiv8")
									 ->setTitle("Customer Info");

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A1', 'Customer ID')
		            ->setCellValue('B1', 'Info')
		            ->setCellValue('C1', 'Date Created');
		$row = 2;
		//re construct data show customer info
		foreach($res2->result_array() as $key => $customer) {
			$this->db->select('label, value');
			$cus_info = $this->db->get_where('h_client_customer_infos', array('client_customer_id' => $customer['uid']));

			$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $customer['uid']);
			$_info = "";

			foreach ($cus_info->result_array() as $info) {
				$_info .= $info['label'] . " : " .$info['value']. " \n"; 
			}

			$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $_info);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, date('M d, Y g:i:s A', strtotime($customer['date_created'])));

			//alignment
			$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)->setWrapText(true);

			$row++;
		}

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Customer Info');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Report.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0


		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}


	private function _page_visitors() {
		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//landing page visitors
		$this->db->select('COUNT(id) as total');
		$this->db->like('logs', '"action":"visitor","page":"landing"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $this->campaign_id));

		//data with offset limit
		$this->db->select('id, date_created');
		$this->db->like('logs', '"action":"visitor","page":"landing"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $this->campaign_id), $limit, $offset);


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}

	public function graphs() {
		$campaign_id = $this->campaign_id;
		$month = date('m');
		$year = date('Y');

		$start_date = date("Y-m-d", strtotime($year.'-'.$month.'-01'));
		$end_date = date("Y-m-t", strtotime($year.'-'.$month.'-01'));

		//no. of page visitor
		$this->db->select('DAY(date_created) AS DAY, COUNT(*) AS total');
		$this->db->like('logs', '"action":"visitor","page":"landing"');
		$this->db->group_by('DAY(date_created)'); 
		$this->db->where('date_created >= "' . $start_date . ' 00:00:00" AND date_created <="' . $end_date . ' 11:59:59"' ); 
		$res1 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of registrants
		$this->db->select('DAY(date_created) AS DAY, COUNT(*) AS total');
		$this->db->like('logs', '"action":"register","page":"register-popup"');
		$this->db->group_by('DAY(date_created)'); 
		$this->db->where('date_created >= "' . $start_date . ' 00:00:00" AND date_created <="' . $end_date . ' 11:59:59"' ); 
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of ios downloads
		$this->db->select('DAY(date_created) AS DAY, COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing\/ios"');
		$this->db->group_by('DAY(date_created)'); 
		$this->db->where('date_created >= "' . $start_date . ' 00:00:00" AND date_created <="' . $end_date . ' 11:59:59"' ); 
		$res3 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of android downloads
		$this->db->select('DAY(date_created) AS DAY, COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing\/android"');
		$this->db->group_by('DAY(date_created)'); 
		$this->db->where('date_created >= "' . $start_date . ' 00:00:00" AND date_created <="' . $end_date . ' 11:59:59"' ); 
		$res4 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of redempions
		$this->db->select('o.name, count(cv.id) as ios, count(cv2.id) as android');
		$this->db->from("h_outlets as o
		LEFT JOIN h_campaign_vouchers_history as cvh ON cvh.outlets_id = o.id AND cvh.status = 'consumed'
		LEFT JOIN h_campaign_vouchers as cv ON cv.id = cvh.campaign_vouchers_id AND cv.channel = 'ios'
		LEFT JOIN h_campaign_vouchers as cv2 ON cv2.id = cvh.campaign_vouchers_id AND cv2.channel = 'android'");
		$this->db->where(array('merchant_id' => 5, 'o.status' =>'Active')); 
		$this->db->group_by('o.id'); 
		$res5 = $this->db->get(); 


		//no. of age per registrant
		$this->db->select('age, count(age) as register');
		$this->db->from("(SELECT hcci.client_customer_id, hcci.value as age
    	FROM h_client_customer_infos as hcci
    	JOIN h_client_customers as hcc ON hcc.id = hcci.client_customer_id
    	JOIN h_campaign_log as hcl on hcl.uid = hcc.id
    	WHERE hcl.campaign_id = $campaign_id and hcci.label = 'age' and hcci.value > 0
		GROUP BY hcci.client_customer_id) as age_group");
		$this->db->group_by('age');
		$res6 = $this->db->get();


		//male and female counts
		$this->db->select('male_count, female_count, (male_count + female_count) as total');
		$this->db->from("(
		SELECT count(value) AS male_count 
	    FROM h_client_customer_infos as hcci 
	    JOIN h_client_customers as hcc ON hcc.id = hcci.client_customer_id 
	    JOIN h_campaign_log as hcl on hcl.uid = hcc.id 
	    WHERE hcl.campaign_id = $campaign_id and hcci.label = 'gender' and hcci.value = 'Male' AND logs LIKE '%{\"action\":\"register\"%'
		) as male_group,
	  	(
		SELECT count(value) AS female_count 
	    FROM h_client_customer_infos as hcci 
	    left JOIN h_client_customers as hcc ON hcc.id = hcci.client_customer_id 
	    left JOIN h_campaign_log as hcl on hcl.uid = hcc.id 
	    WHERE hcl.campaign_id = $campaign_id and hcci.label = 'gender' and hcci.value = 'female' AND logs LIKE '%{\"action\":\"register\"%'
	    ) as female_group");
		$res7 = $this->db->get();

		$graphs = array(
			'visitors' => array(),
			'registrants' => array(),
			'ios' => array(),
			'android' => array(),
			'labels' => array(),
			'age' => array(),
			'age_labels' =>array(), //x-axis,
			'gender_total' =>array(), //x-axis,
			'male_total' =>array(), 
			'female_total' =>array(),
			'redemptions' => array('ios' => array(), 'android' => array()) 
		);

		$visitor_result = array();
		$registrant_result = array();
		$ios_downloads = array();
		$android_downloads = array();
		$age_result = array();

		//need to create an array to save result
		foreach ($res1->result_array() as $v) {
			$visitor_result[$v['DAY']] = $v['total'];
		}

		//need to create an array to save result
		foreach ($res2->result_array() as $v) {
			$registrant_result[$v['DAY']] = $v['total'];
		}

		//need to create an array to save result
		foreach ($res3->result_array() as $v) {
			$ios_downloads[$v['DAY']] = $v['total'];
		}

		//need to create an array to save result
		foreach ($res4->result_array() as $v) {
			$android_downloads[$v['DAY']] = $v['total'];
		}

		//redemptions per outlet per channel
		foreach($res5->result_array() as $v) {
			$graphs['redemptions']['ios'][] = $v['ios'];
			$graphs['redemptions']['android'][] = $v['android'];
		}

		//need to create an array to save result
		foreach ($res6->result_array() as $v) {
			$graphs['age_labels'][] = $v['register'];
			$graphs['age'][] = $v['age'];
		}

		foreach ($res7->result_array() as $v) {
			$graphs['gender_total'][] = $v['total'];
			$graphs['male_total'][] = $v['male_count'];
			$graphs['female_total'][] = $v['female_count'];
		}


		for($d = 1; $d <= 31; $d++)
		{
		    $time=mktime(12, 0, 0, $month, $d, $year);          
		    if (date('m', $time) == $month) {
		    	$graphs['labels'][] = date('M d, Y', $time);

		    	//vistors
		    	if (isset($visitor_result[$d])) {
		    		//get total and save into array
		    		$graphs['visitors'][] = $visitor_result[$d];
		    	} else {
		    		$graphs['visitors'][] = 0;
		    	}

		    	//registrants
		    	if (isset($registrant_result[$d])) {
		    		//get total and save into array
		    		$graphs['registrants'][] = $registrant_result[$d];
		    	} else {
		    		$graphs['registrants'][] = 0;
		    	}

		    	//ios downloads
		    	if (isset($ios_downloads[$d])) {
		    		//get total and save into array
		    		$graphs['ios'][] = $ios_downloads[$d];
		    	} else {
		    		$graphs['ios'][] = 0;
		    	}

		    	//ios downloads
		    	if (isset($android_downloads[$d])) {
		    		//get total and save into array
		    		$graphs['android'][] = $android_downloads[$d];
		    	} else {
		    		$graphs['android'][] = 0;
		    	}
		    }
		}

		// echo '<pre>';
		// print_r($graphs);
		// exit;

		$data['graphs'] = json_encode($graphs);

		$data['sidebar_view'] = 'client/uemura/sidebar';

		$this->load->view('client/uemura/reports/graphs', $data);
	}

	private function _registrants() {
		$campaign_id = $this->campaign_id;

		//dataTables format
		$columns = array(
			0 => 'uid',
			1 => 'info',
			2 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//landing page visitors
		$this->db->select('COUNT(id) as total');
		$this->db->like('logs', '"action":"register","page":"register-popup"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//data with offset limit
		$this->db->select('id, uid, date_created, uid AS operations');
		$this->db->like('logs', '"action":"register","page":"register-popup"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id), $limit, $offset);

		$result_data =array();

		//re construct data show customer info
		foreach($res2->result_array() as $key => $customer) {
			$this->db->select('label, value');
			$cus_info = $this->db->get_where('h_client_customer_infos', array('client_customer_id' => $customer['uid']));

			$result_data[$key]['uid'] = $customer['uid'];
			$result_data[$key]['date_created'] = $customer['date_created'];
			$result_data[$key]['info'] = '';

			foreach ($cus_info->result_array() as $info) {
				$result_data[$key]['info'] .= $info['label'] . ' : ' .$info['value']. '<br />'; 
			}

		}


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $result_data
		);

		return $data;

	}


	private function _download_from_android() {
		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//no. of download voucher android
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing\/android"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $this->campaign_id));

		//data with offset limit
		$this->db->select('id, date_created');
		$this->db->like('logs', '"action":"download","page":"landing\/android"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $this->campaign_id), $limit, $offset);


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}


	private function _download_from_ios() {
		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//no. of download voucher android
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing\/ios"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $this->campaign_id));

		//data with offset limit
		$this->db->select('id, date_created');
		$this->db->like('logs', '"action":"download","page":"landing\/ios"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $this->campaign_id), $limit, $offset);


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}


	private function _redemptions($channel) {
		//dataTables format
		$columns = array(
			0 => 'cv.customer_id',
			1 => 'o.name',
			2 => 'cvh.date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//no of redeem ios
		$this->db->select('COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id AND cvh.outlets_id = o.id');
		$res = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci, h_outlets as o', 
			array(
				'campaign_id' => $this->campaign_id,
				'cvh.status' => 'consumed',
				'cv.channel' => $channel,
		));

		//data with offset limit
		$this->db->select('cv.customer_id, o.name as outlet_name, cvh.date_created');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id AND cvh.outlets_id = o.id');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci, h_outlets as o', 
			array(
				'campaign_id' => $this->campaign_id,
				'cvh.status' => 'consumed',
				'cv.channel' => $channel,
		));


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}
}
