<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($campaign_id = 6)
	{	
		$is_log = $this->session->userdata('logged_in');
		$weekRangeWhere[0] = '1 = 1';
		$weekRangeWhere[1] = '1 = 1';

		if ($is_log == NULL && $is_log == FALSE) {
            return redirect(base_url('client/login'));
        }

        $data['sidebar_view'] = 'client/uemura/sidebar';

        if (isset($_GET['week'])) {
        	list($startDate, $endDate) = explode('-', $_GET['week']);
        	$startDate = date_format(date_create($startDate), 'Y-m-d');
        	$endDate = date_format(date_create($endDate), 'Y-m-d');
        	$weekRangeWhere[0] = "date_created BETWEEN '". $startDate . " 00:00:00' AND '". $endDate. " 23:59:59'";
        	//for ambigious column
        	$weekRangeWhere[1] = "cvh.date_created BETWEEN '". $startDate . " 00:00:00' AND '". $endDate. " 23:59:59'";
        }

		//no. of page visitor
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"visitor","page":"landing"');
		$this->db->where($weekRangeWhere[0]);
		$res1 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of registrant
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"register","page":"register-popup"');
		$this->db->where($weekRangeWhere[0]);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of download voucher ios
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing\/ios"');
		$this->db->where($weekRangeWhere[0]);
		$res3 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of download voucher android
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing\/android"');
		$this->db->where($weekRangeWhere[0]);
		$res4 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no of redeem ios
		$this->db->select('COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id');
		$this->db->where($weekRangeWhere[1]);
		$res5 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'campaign_id' => $campaign_id,
				'cvh.status' => 'consumed',
				'cv.channel' => 'ios',
		));

		//no redeem android
		$this->db->select('COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id');
		$this->db->where($weekRangeWhere[1]);
		$res6 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'campaign_id' => $campaign_id,
				'cvh.status' => 'consumed',
				'cv.channel' => 'android',
		));

		//no. of page visitor from FB
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"visitor","page":"landing","from-channel":"fb"');
		$this->db->where($weekRangeWhere[0]);
		$res7 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		$data['total_page_visitor'] = $res1->row()->total;
		$data['total_register'] = $res2->row()->total;
		$data['total_ios_downloads'] = $res3->row()->total;
		$data['total_android_downloads'] = $res4->row()->total;
		$data['total_ios_redemption'] = $res5->row()->total;
		$data['total_android_redemption'] = $res6->row()->total;
		$data['total_page_visitor_fb'] = $res7->row()->total;

		$this->load->view('client/uemura/index', $data);
	}

	public function logs($campaign_id = 6) {
    	$action = $_POST['action'];
    	$page = $_POST['page'];

    	//check action visit on log field json search using like clause
    	if ($action == 'visitor') {
    		$from_channel = $_POST['from_channel'];

    		$this->db->like('logs', '"action":"'. $action.'"');
    		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id, 'uid' => $_POST['visit_id']));
    		
    		//if there is no record check latest uid
    		if ($res->num_rows() == 0 ) {
    			$this->db->like('logs', '"action":"'. $action.'"');
    			$this->db->order_by('uid', 'DESC');
    			$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id), 1);

    			$log = array(
					'action' => 'visitor',
					'page' => $page,
					'from-channel' => $from_channel,
				);

    			//if no record create first record
    			if ($res->num_rows() == 0 ) {
    				$data = array(
		                'campaign_id' => $campaign_id,
		                'uid' => 1,
		                'logs' => json_encode($log),
		                'date_created' => date("Y-m-d H:i:s"),
		            );

		            $this->db->insert('h_campaign_log', $data);

		            echo json_encode(array('visit_id' => 1));
		           	return;
    			} else {
    				//get latest id then add new one
    				$data = array(
		                'campaign_id' => $campaign_id,
		                'uid' => $res->row()->uid + 1,
		                'logs' => json_encode($log),
		                'date_created' => date("Y-m-d H:i:s"),
		            );

		            $this->db->insert('h_campaign_log', $data);

		            echo json_encode(array('visit_id' => $data['uid']));
		           	return;
    			}
    		} else {
    			//do nothing
    			echo json_encode(array('visit_id' => 0));
	           	return;
    		}
    	}


    	//check action visit on log field json search using like clause
    	if ($action == 'register') {
    		parse_str($_POST['data'], $customer_data);//convert serialize string to array

    		$this->db->like('logs', '"action":"'. $action.'"');
    		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id, 'uid' => 0));
    		
    		//if there is no record create log
    		if ($res->num_rows() == 0 ) {
    			$log = array(
					'action' => $action,
					'page' => $page,
					'from-visit-id' => $_POST['visit_id']
				);

				$date_created = date("Y-m-d H:i:s");

				//create customer record
				$data = array(
		        	'client_id' => 5, // replace with get client id 
		        	'date_created' => $date_created,
		        	'date_updated' => $date_created
		        );

				$this->db->insert('h_client_customers', $data);
				$uid = $this->db->insert_id();

				//insert row field value of the customer
				foreach ($customer_data as $key => $value) {
					if ($key == 'terms_and_conditions') continue;
					//create customer info record
					$data = array(
			        	'client_customer_id' => $uid, 
			        	'label' => $key, 
			        	'value' => $value, 
			        	'date_created' => $date_created,
			        	'date_updated' => $date_created
			        );

					$this->db->insert('h_client_customer_infos', $data);
				}

				//create log
				$data = array(
	                'campaign_id' => $campaign_id,
	                'uid' => $uid ,
	                'logs' => json_encode($log),
	                'date_created' => $date_created,
	            );

	            $this->db->insert('h_campaign_log', $data);

	            echo json_encode(array('visit_id' => $uid));
	           	return;
    		} else {
    			//do nothing
    			echo json_encode(array('visit_id' => 0));
	           	return;
    		}
    	}


    	//check action visit on log field json search using like clause
    	if ($action == 'download') {
    		$channel = $_POST['channel'];
    		$items = array(6 => array());

    		$items[6] = array(
				'item_id' => 6,
				'campaign_items_id' => 9
			);
    		
    		$this->db->like('logs', '"action":"'. $action.'"');
    		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id, 'uid' => $_POST['visit_id']));
    		
    		//if there is no record create log
    		if ($res->num_rows() == 0 ) {
    			$log = array(
					'action' => $action,
					'page' => $page,
				);

				$date_created = date("Y-m-d H:i:s");

				//create campaign voucher
				$data = array(
		        	'customer_id' => $_POST['visit_id'], 
		        	'campaign_items_id' => $items[$campaign_id]['campaign_items_id'], //from campaign item table 
		        	'item_id' => $items[$campaign_id]['item_id'], //from items table 
		        	'type' => 'consumable',  
		        	'channel' => $channel,  
		        	'date_created' => $date_created,
		        	'date_updated' => $date_created
		        );

				$this->db->insert('h_campaign_vouchers', $data);
				$campaign_voucher_id = $this->db->insert_id();

				//create campaign voucher history
				$data = array(
		        	'campaign_vouchers_id' => $campaign_voucher_id, 
		        	'outlets_id' => NULL, 
		        	'status' => 'unused',
		        	'date_created' => $date_created,
		        );

				$this->db->insert('h_campaign_vouchers_history', $data);

				//create log
				$data = array(
	                'campaign_id' => $campaign_id,
	                'uid' => $_POST['visit_id'] ,
	                'logs' => json_encode($log),
	                'date_created' => $date_created,
	            );

	            $this->db->insert('h_campaign_log', $data);

	            echo json_encode(array('visit_id' => $_POST['visit_id']));
	           	return;
    		} else {
    			//do nothing
    			echo json_encode(array('visit_id' => 0));
	           	return;
    		}
    	}

	}

	public function redeem($campaign_id = 6) {
		$items = array(6 => array());
    	$date_created = date("Y-m-d H:i:s");

    	$action = $_POST['action'];
    	$page = $_POST['page'];
    	$customer_id = $_POST['visit_id'];
    	$code = $_POST['code'];
    	$log = array(
			'action' => $action,
			'page' => $page,
		);


		$items[6] = array(
			'item_id' => 6,
			'campaign_items_id' => 9
		);


    	$res = $this->db->get_where('h_campaign_vouchers', array('customer_id' => $customer_id, 'campaign_items_id' => $items[$campaign_id]['campaign_items_id']));
    	$campaign_voucher_id = $res->row()->id;

    	$res2 = $this->db->get_where('h_campaign_vouchers_history', array('campaign_vouchers_id' => $campaign_voucher_id, 'status' => 'consumed'));

    	//If already redeemed
    	if ($res2->num_rows() > 0) {
    		echo json_encode(array(
				'success' => FALSE,
				'outlet' => TRUE,
			));
    		return;
    	}

    	//check outlet code if exists
    	$res3 = $this->db->get_where('h_outlets', array('merchant_id' => 5, 'status' => 'Active', 'code' => NULL));

    	if ($res3->num_rows() == 0) {
    		echo json_encode(array(
				'success' => TRUE,
				'outlet' => FALSE,
			));
    		return;
    	}


    	//create campaign voucher history
		$data = array(
        	'campaign_vouchers_id' => $campaign_voucher_id, 
        	'outlets_id' => $res3->row()->id, 
        	'status' => 'consumed',
        	'date_created' => $date_created,
        );

		$this->db->insert('h_campaign_vouchers_history', $data);

		//create log
		$data = array(
            'campaign_id' => $campaign_id,
            'uid' => $customer_id ,
            'logs' => json_encode($log),
            'date_created' => $date_created,
        );

        $this->db->insert('h_campaign_log', $data);

        echo json_encode(array(
			'success' => TRUE,
			'outlet' => TRUE,
		));
		return;

    } 
}
