<?php
/**
 * Copyright 2013 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class to generate example Loyalty class and objects
 */
class Loyalty {


  /**
   * Generates a Loyalty Object
   *
   * @param String $issuerId Wallet Object merchant account id.
   * @param String $classId Wallet Class that this wallet object references.
   * @param String $objectId Unique identifier for a wallet object.
   * @return Object $wobObject Loyaltyobject resource.
   */
  public static function generateLoyaltyObject($issuerId, $classId, $objectId, $url) {
    
    // Define text module data.
    $textModulesData = array(
        array(
            'header' => '',
            'body' => 'and click below to redeem!'
        )
    );
    
    // Define links module data.
    $linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
    $uris = array (
        array(
            'uri' => $url,
            'kind' => 'walletobjecs#uri',
            'description' => 'REDEEM NOW'
        )
    );
    $linksModuleData->setUris($uris);
    
    // Define info module data.
    $infoModuleData = new Google_Service_Walletobjects_InfoModuleData();
    $infoModuleData->setHexBackgroundColor('#442905');
    $infoModuleData->setHexFontColor('#F8EDC1');
    $infoModuleData->setShowLastUpdateTime(true);
    // Messages to be displayed.
    $messages = array(
        array(
            'actionUri' => array(
            'kind' => 'walletobjects#uri',
            'uri' => 'http://www.isseymiyakeparfums.com/en'
            ),
            'header' => '',
            'body' => 'Please present this voucher to the nearest counter\'s store staff ',
            'kind' => 'walletobjects#walletObjectMessage'
        ),
        array(
            'actionUri' => array(
            'kind' => 'walletobjects#uri',
            'uri' => 'http://www.isseymiyakeparfums.com/en'
            ),
            'header' => '',
            'body' => 'While Stocks Last.',
            'kind' => 'walletobjects#walletObjectMessage'
        ),
        array(
            'actionUri' => array(
            'kind' => 'walletobjects#uri',
            'uri' => 'http://www.isseymiyakeparfums.com/en'
            ),
            'header' => '',
            'body' => 'Discover the new l\'Eau d\'Issey Pure.',
            'kind' => 'walletobjects#walletObjectMessage'
        )
    );
    
    // Create wallet object.
    $wobObject = new Google_Service_Walletobjects_LoyaltyObject();
    $wobObject->setClassId($issuerId.".".$classId);
    $wobObject->setId($issuerId.".".$objectId);
    $wobObject->setState('active');
    $wobObject->setVersion(1);
    $wobObject->setInfoModuleData($infoModuleData);
    $wobObject->setLinksModuleData($linksModuleData);
    $wobObject->setTextModulesData($textModulesData);
//    $wobObject->setAccountName('Jane Doe');
//    $wobObject->setAccountId('REDEEM');
    $wobObject->setMessages($messages);
    return $wobObject;
  }
}
