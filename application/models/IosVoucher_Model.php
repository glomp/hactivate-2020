<?php
/**

TODO: replace mytable, remove all file_put_contents after

**/
class IosVoucher_Model extends CI_Model {
     
    public function __construct($url = "")
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function registerDevice($deviceLibraryIdentifier, $passTypeIdentifier, $serialNumber, $pushToken, $version)
    {
    	
		// save to database  
        $data = array(
        	'customer_id' => 1,
        	'campaign_id' => 1,
        	'device_id' => $deviceLibraryIdentifier,
        	'pass_type_id' => $passTypeIdentifier,
        	'serial_number' =>  $serialNumber,
        	'push_token' => $pushToken,
        	'version' => $version,
        	'date_created' => date("Y-m-d H:i:s"),
        	'date_updated' => date("Y-m-d H:i:s")
        );

        $this->db->insert('h_campaign_ios_vouchers', $data);
        /**
		file_put_contents("json.txt", "Add" . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents("json.txt", "URL: " . $this->url . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents("json.txt", "Request: " . $this->requestType . PHP_EOL, FILE_APPEND | LOCK_EX); 
		file_put_contents("json.txt", "Push Token: " . $pushtoken . PHP_EOL, FILE_APPEND | LOCK_EX); 
		file_put_contents("json.txt", "Device: " . $this->deviceLibraryIdentifier . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents("json.txt", "Identifier: " . $this->passTypeIdentifier  . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents("json.txt", "Serial Number: " . $this->serialNumber  . PHP_EOL . PHP_EOL, FILE_APPEND | LOCK_EX);
        **/
    }
    
    function deleteDevice ($deviceLibraryIdentifier, $passTypeIdentifier, $serialNumber) 
    {
		
        $this->db->where('device_id', $deviceLibraryIdentifier);
        $this->db->where('pass_type_id', $passTypeIdentifier);
        $this->db->where('serial_number', $serialNumber);

        $this->db->update('h_campaign_ios_vouchers', array(
            'status' => 'D',
            'date_updated' => date("Y-m-d H:i:s")
        ));
        //$this->db->delete('h_campaign_ios_vouchers');
        
        /**
		file_put_contents("json.txt", "Delete" . PHP_EOL, FILE_APPEND | LOCK_EX); 
		file_put_contents("json.txt", "URL: " . $this->url . PHP_EOL, FILE_APPEND | LOCK_EX); 
		file_put_contents("json.txt", "Request: " . $this->requestType . PHP_EOL, FILE_APPEND | LOCK_EX); 
		file_put_contents("json.txt", "Device: " . $this->deviceLibraryIdentifier . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents("json.txt", "Identifier: " . $this->passTypeIdentifier  . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents("json.txt", "Serial Number: " . $this->serialNumber  . PHP_EOL . PHP_EOL, FILE_APPEND | LOCK_EX);
        **/
	}
	
	function getSerialNumbers ($passTypeIdentifier, $deviceId) 
    {  
        $sql = "SELECT * FROM h_campaign_ios_vouchers WHERE pass_type_id = ? AND device_id = ?";
         
        $query = $this->db->query($sql, array($passTypeIdentifier, $deviceId));
        $array_result = array();

        foreach ($query->result() as $row)
        {
            $array_result[] = $row->serial_number;
        }
        
        
        return $array_result;
        
        /**
		file_put_contents("json.txt", "Latest" . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents("json.txt", "URL: " . $this->url . PHP_EOL, FILE_APPEND | LOCK_EX); 
		file_put_contents("json.txt", "Request: " . $this->requestType . PHP_EOL, FILE_APPEND | LOCK_EX); 
		file_put_contents("json.txt", "Version: " . $this->version . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents("json.txt", "Identifier: " . $this->passTypeIdentifier . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents("json.txt", "Serial Number: " . $this->serialNumber . PHP_EOL, FILE_APPEND | LOCK_EX);
        **/
	}
	
    function getLatestPass ()
    {
        
    }
    
	function logs() 
    {
		$data = json_decode(file_get_contents("php://input"));
		$string = $data->logs;
		$pushstring = print_r($data, true);
		
		file_put_contents("json.txt", " " . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents("json.txt", "Log" . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents("json.txt", "URL: " . $this->url . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents("json.txt", "String: " . $string . PHP_EOL, FILE_APPEND | LOCK_EX); 
		file_put_contents("json.txt", "Raw: " . $pushstring . PHP_EOL, FILE_APPEND | LOCK_EX);
	}
    
    function getVoucherJSON($campaign_id)
    {
        $sql = "SELECT * FROM h_campaign_voucher_properties WHERE campaign_id = ? AND device_type = 'ios'";
         
        $query = $this->db->query($sql, array($campaign_id));
        $result = "";
 
        foreach ($query->result_array() as $row)
        {
            $result = $row;
        }
        
        
        return json_decode($result["properties"], true);
    }
}