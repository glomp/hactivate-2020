<?php $this->load->view('admin/global/header'); ?>

				<!-- page content -->
				<div class="right_col" role="main">

					<div class="row">
			           <a href="<?php echo site_url('admin/user/add'); ?>" class="btn btn-primary btn-md">Add</a>  
			         </div>

			         <div class="row">
					<table id="example" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Firstname</th>
								<th>Lastname</th>
								<th>Email Address</th>
								<th>Gender</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Firstname</th>
								<th>Lastname</th>
								<th>Email Address</th>
								<th>Gender</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
							<?php foreach($users as $user) { ?>
							<tr>
								<td><?php echo $user->user_firstname ?></td>
								<td><?php echo $user->user_lastname ?></td>
								<td><?php echo $user->username ?></td>
								<td><?php echo $user->user_gender ?></td>
								<td><a href="<?php echo site_url('admin/user/edit'); ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><a>
								<a href="<?php echo site_url('admin/user/delete'); ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span><a></td>
							</tr>

							<?php } ?>
							
						</tbody>
					</table>

					 </div>

				</div>
				<!-- /page content -->

				<!-- footer content -->
				<?php $this->load->view('admin/global/footer'); ?>

		<!-- Custom Theme Scripts -->
		<script src="<?php echo base_url('assets/datatables/jquery.dataTables.min.css'); ?>"></script>
		<script src="<?php echo base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.js'); ?>"></script>

		<!-- Flot -->
		<script>
			

			$(document).ready(function() {
			    $('#example').DataTable( {
			       // "ajax": '../ajax/data/arrays.txt'
			    } );
			} );
		</script>

