
  <body>
    <div class="overlay" style="display:none;position: fixed;height: 100%;width: 100%;background-color: rgba(33, 31, 31, 0.5);top: 0px;z-index: 2;" ></div>

    <div style='display:none' id="download-ios">
         <iframe id="frame" src="" width="100%" height="300"></iframe>
    </div>


    <div id="overlay-2" style="display:none;padding: 10px;position: absolute;top: 123px;width: 100%;z-index: 3" class="row-fluid">
        <div style="background-color: #046d32;height: 208px;padding: 50px;" class="col s12">
            <div id="apple-btn" style="display:none;margin-top: 22px;" class="col s12">
                <div style="color:white;font-size: 18px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Apple</div>
            </div>
        </div>
    </div>

    <div id="overlay-3" style="display:none;padding: 10px;position: absolute;top: 123px;width: 100%;z-index: 3" class="row-fluid">
        <div style="background-color: #505153;padding: 50px;" class="col s12">
            <label style="color:white;">Test Push Notification by tapping "Test Push!" button</label>
          
            <a style="width: 100%" target="_blank" class="btn btn-default" id="test-push" />Test Push!</a>
            <br /><br />
            
            <button style="width: 100%" class="btn btn-default" id="close-push-dialog" />Cancel</button>
        </div>
    </div>


    <div id="download-ios-note" style="display:none;padding: 10px;position: absolute;top: 40px;width: 100%;z-index: 3;" class="row-fluid">
        <div style="background-color: #00584b;color:white;padding: 30px;" >
            <div class="row">
              <div class="col-md-12" align="center">
                <img style="width: 100px; margin-bottom: 10px;" class="img-responsive" src="<?php echo base_url('assets/images/bodyshop-b/thebodyshop-logo.jpg'); ?>" >
              </div> 
            </div>
            <div class="col s12">Please enter your details to receive your voucher.</div>
            <br />
            <div style="font-size: 10px;display: none;" class="error-msg alert alert-danger">
                <ul style="padding-left: 20px;">
                    <li>Age required</li>
                    <li>Gender required</li>
                    <li>Mobile required</li>
                    <li>Please check the terms and conditions</li>
                </ul>
            </div>
            <form class="register-form" id="register-form">
                <select style="border: 1px solid black;color: #797878;width: 100%;margin-bottom: 10px;" name="gender">
                  <option value=''>--Gender--</option>
                  <option value='Male'>Male</option>
                  <option value='female'>Female</option>
                </select>
                <input required style="width: 100%;height: 30px" name="age" type="text" placeholder="Age"/>
                <input style="width: 100%;height: 30px" name="mobile_number" type="text" placeholder="Mobile Number"/>

                <div style="font-size: 11px;">
                  <label><input id="subscribe-news" value = "Yes" name="subscribe_news" type="checkbox"> Yes, sign me up to receive news from The Body Shop</label>
                  <label><input checked id="terms-and-conditions" value = "Yes" name="terms_and_conditions" type="checkbox"> Yes, I have read and agree to the 
                    <a style="color:white;" target ="_blank" href = "<?php echo base_url('landing/page/bodyshop-b/terms'); ?>" ><u>Terms & Conditions</u></a> of this voucher
                  </label>
                </div>
            </form>
            <div class="button-container">
                <div class="apple-note-btn" class="col s12">
                    <div style="color:white;font-size: 12px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Submit</div>
                </div>
                <br />
                <div class="apple-skip-btn" class="col s12">
                    <div style="color:white;font-size: 9px;text-align: center;width: 100%;border-radius: initial;font-weight: bold;text-decoration: underline;margin-top: -10px;" >Skip</div>
                </div>
            </div>
        </div>
    </div>


    <div id="download-ios-note-2" style="display:none;padding: 10px;position: absolute;top: 60px;width: 100%;z-index: 3" class="row-fluid">
        <div style="background-color: #00584b; color:white; padding: 50px;" >
            <div class="row">
                <div class="col-md-12" align="center">
                  <img style="width: 100px; margin-bottom: 10px;" class="img-responsive" src="<?php echo base_url('assets/images/bodyshop/thebodyshop-logo.jpg'); ?>" >
                </div> 
            </div>
            <div class="col s12">Your voucher will appear next. Please click “Add” in order to save it to your Apple Wallet.</div>
            <br />
            <div class="button-container">
                <div class="apple-note-btn-2" class="col s12">
                    <div style="color:white;font-size: 12px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Continue</div>
                </div>
            </div>
        </div>
    </div>


    <div id="download-android-note" style="display:none;padding: 10px;position: absolute;top: 40px;width: 100%;z-index: 3;" class="row-fluid">
        <div style="background-color: #00584b; color:white; padding: 50px;" >
            <div class="row">
              <div class="col-md-12" align="center">
                <img style="width: 100px; margin-bottom: 10px;" class="img-responsive" src="<?php echo base_url('assets/images/bodyshop-b/thebodyshop-logo.jpg'); ?>" >
              </div> 
            </div>
            <div class="col s12">Please enter your details to receive your voucher.</div>
            <br />
            <div style="font-size: 10px;display: none;" class="error-msg alert alert-danger">
                <ul style="padding-left: 20px;">
                    <li>Age required</li>
                    <li>Gender required</li>
                    <li>Mobile required</li>
                    <li>Please check the terms and conditions</li>
                </ul>
            </div>
            <form class="register-form" id="register-form-android">
                <select style="border: 1px solid black;color: #797878;width: 100%;margin-bottom: 10px;" name="gender">
                  <option value=''>--Gender--</option>
                  <option value='Male'>Male</option>
                  <option value='female'>Female</option>
                </select>
                <input required style="width: 100%;height: 30px" name="age" type="text" placeholder="Age"/>
                <input style="width: 100%;height: 30px" name="mobile_number" type="text" placeholder="Mobile Number"/>

                <div style="font-size: 11px;">
                  <label><input id="subscribe-news" value = "Yes" name="subscribe_news" type="checkbox"> Yes, sign me up to receive news from The Body Shop</label>
                  <label><input checked id="terms-and-conditions" value = "Yes" name="terms_and_conditions" type="checkbox"> Yes, I have read and agree to the 
                    <a style="color:white;" target ="_blank" href = "<?php echo base_url('landing/page/bodyshop-b/terms'); ?>" ><u>Terms & Conditions</u></a> of this voucher
                  </label>
                </div>
            </form>
            <div class="button-container">
                <div class="android-note-btn" class="col s12">
                  <div style="color:white;font-size: 12px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Submit</div>
                </div>
                <br />
                <div class="android-skip-btn" class="col s12">
                    <div style="color:white;font-size: 9px;text-align: center;width: 100%;border-radius: initial;font-weight: bold;text-decoration: underline;margin-top: -10px;" >Skip</div>
                </div>
            </div>
        </div>
    </div>


    <div id="download-android-note-2" style="display:none;padding: 10px;position: absolute;top: 60px;width: 100%;z-index: 3" class="row-fluid">
        <div style="background-color: #00584b; color:white; padding: 50px;" >
            <div class="row">
                <div class="col-md-12" align="center">
                  <img style="width: 100px; margin-bottom: 10px;" class="img-responsive" src="<?php echo base_url('assets/images/bodyshop/thebodyshop-logo.jpg'); ?>" >
                </div> 
            </div>
            <div class="col s12">Your voucher will appear next. Please sign into your google account if necessary and click "Save" in order to save it to your Android Pay wallet.</div>
            <br />
            <div class="button-container">
                <div id="android-btn" style="margin-top: 22px;" class="col s12">
                <div style="color:white;font-size: 18px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Continue</div>
            </div>
            <div align="center" id="android-pay-btn" style="display:none;margin-top: 22px; border: 1px solid white;background: rgb(38, 51, 57); padding: 5px;" class="col s12"></div>            </div>
        </div>
    </div>


    <div id="share" style="display:none;padding: 10px;position: absolute;top: 130px;width: 100%;z-index: 3;text-align: center;" class="row-fluid">
        <div style="background-color: #00584b; color:white; padding: 50px;" >
            <div class="col s12">Share</div>
            <div class="col s12">
                <!-- <a href="https://www.facebook.com/sharer/sharer.php?u=<?php //echo base_url('landing/page/bodyshop'); ?>" target="_blank">FB</a> -->
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url('landing/page/bodyshop-b'); ?>" target="_blank">
                  <img style="width: 35px;" src = "<?php echo base_url('assets/images/bodyshop-b/fblogo.png') ?>" />
                </a>
                <a href="whatsapp://send?text=Enjoy The Body Shop Truth or Dare gift set offer like I did! Go to http://truthdare.info on your mobile to receive your exclusive voucher." data-action="share/whatsapp/share">
                  <img style="width: 35px;" src = "<?php echo base_url('assets/images/bodyshop-b/whatsapp.png') ?>" />
                </a>
                <a class = "hidden" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url('landing/page/bodyshop-b?r=linkedin'); ?>" target="_blank">
                  <img style="width: 35px;" src = "<?php echo base_url('assets/images/bodyshop-b/linkedinlogo.png') ?>" />
                </a>
                <a class="hidden"> href="http://twitter.com/share?text=The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop&url=<?php echo base_url('landing/page/bodyshop-b?r=twitter'); ?>"" target="_blank">
                  <img style="width: 35px;" src = "<?php echo base_url('assets/images/bodyshop-b/twitterlogo.png') ?>" />
                </a>
            </div>
        </div>
    </div>



    <div id="please-wait" style="display:none;padding: 10px;position: absolute;top: 60px;width: 100%;z-index: 3" class="row-fluid">
        <div style="background-color: #00584b; color:white; padding: 50px;" >
            <div class="col s12">Please wait...</div>
        </div>
    </div>

    <div id="desktop-note" style="display:none;padding: 10px;position: absolute;top: 60px;width: 100%;z-index: 3" class="row-fluid">
        <div style="background-color: #00584b; color:white; padding: 50px;" >
            <div class="col s12">In order to download the voucher, please go to http://truthdare.info on your mobile.</div>
        </div>
    </div>

    <div id="fb-note" style="display:none;padding: 10px;position: absolute;top: 60px;width: 100%;z-index: 3" class="row-fluid">
        <div style="background-color: #00584b; color:white; padding: 50px;" >
            <div class="col s12">
              In order to download the voucher, please open this link using your default browser.
            </div>
        </div>
    </div>



  </body>




<script>
    var base_url = '<?php echo base_url(); ?>';
    var channel = '<?php echo (isset($_GET['channel'])) ? $_GET['channel'] : ''; ?>';

    function isFacebookApp() {
        var ua = navigator.userAgent || navigator.vendor || window.opera;

        return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1);
    }

    if ( isFacebookApp()) {
        $('#fb-note').toggle();
        throw new Error("Something went badly wrong!"); //just to finish execution
    }


    function download(form) {
      $('#please-wait').toggle(); //show
      if (localStorage.getItem('bodyshop_b_customer_id') === null) {
            //create new record
            $.ajax({
              method: "POST",
              url: '<?php echo base_url("client/bodyshop-b/dashboard/logs"); ?>',
              dataType: 'json',
              data: { 
                visit_id: localStorage.getItem('bodyshop_b_visit_id'),
                action: 'register',
                page: 'register-popup',
                data: $(form).serialize()
              },
              success : function(r) {
                if (r.visit_id > 0 ) {
                    $('#please-wait').toggle(); //hide
                    localStorage.setItem('bodyshop_b_customer_id', r.visit_id);

                    if (getMobileOperatingSystem() == 'iOS') {
                        $('#apple-btn').click();
                        return;
                    }

                    if (getMobileOperatingSystem() == 'Android') {
                        init();
                        return 
                    }
                    
                }
              }
            });
        } else {
            alert('Voucher already registered.');
        }
    }

    /**
     * Determine the mobile operating system.
     * This function returns one of 'iOS', 'Android', 'Windows Phone', or 'unknown'.
     *
     * @returns {String}
     */
    function getMobileOperatingSystem() {
      var userAgent = navigator.userAgent || navigator.vendor || window.opera;

          // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            return "iOS";
        }

        return "unknown";
    }


    function getUrlVars(url) {
        var hash;
        var myJson = {};
        var hashes = url.slice(url.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            myJson[hash[0]] = hash[1];
        }
        return myJson;
    }

    if (getMobileOperatingSystem() == 'unknown') {
        $('.overlay, #desktop-note').toggle();
        throw new Error("Something went badly wrong!"); //just to finish execution
    }

    function limit_breaker() {
       var source = new EventSource('<?php echo base_url('iosVoucher/checkVoucherLimit/4'); ?>');

       source.addEventListener('message', function(e) {
        var data = JSON.parse(e.data);

        if (getMobileOperatingSystem() == 'Android') {
          if (data.android < 14950) {
              //no need to continue below alert limit
              return source.close();//close
          }

          if (data.android == 15000) {
                alert('Promo ended!');

                //$('.overlay, #download-android-note').toggle();

                source.close();
                return;
            }
        }

        if (getMobileOperatingSystem() == 'iOS') {
            if (data.ios < 14950) {
                //no need to continue below alert limit
                return source.close();//close
            }

            if (data.ios == 15000) {
                alert('Promo ended!');

                //$('.overlay, #download-ios-note').toggle();

                source.close();
                return;
            }
        }


      }, false);
    }

    //run voucher limit checker
    limit_breaker();


    /**
     * Save to android pay voucher success handler.
     */
    var successHandler = function (params) {
        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/bodyshop-b/dashboard/logs"); ?>',
          dataType: 'json',
          data: { 
            visit_id: localStorage.getItem('bodyshop_b_customer_id'),
            action: 'download',
            channel: 'android',
            page: 'landing/android'
          },
          beforeSend: function() {
            clicked = true;
          },
          success : function(r) {
            clicked = false; //open the function

            if (r.visit_id > 0 ) {
                $('.overlay, #download-android-note-2').toggle();
                showShareDialog();
                return false;
            } else {
                alert('Voucher already downloaded.');
            }
          }
        });
        
    }

    /**
     * Save to Wallet failure handler.
     */
    var failureHandler = function (params) {
        alert('Failed to save voucher');            
    }

    /**
     * Initialization function android pay
     */
    function init() {
        var loyaltyJwtUrl = base_url + 'androidvoucher/getbodyshoptruthordarejwt/' + localStorage.getItem('bodyshop_b_customer_id');
        
        $.when(
            // Get jwt of offer object and render 'Get offer' wallet button.
            $.get(loyaltyJwtUrl, function (data) {

              // gapi.savetoandroidpay.render("#android-pay-btn",{
              //   "jwt": data,
              //   "onsuccess": "successHandler",
              //   "onfailure": "failureHandler"
              // });

              saveToAndroidPay = document.createElement('g:savetoandroidpay');
              saveToAndroidPay.setAttribute('jwt', data);
              saveToAndroidPay.setAttribute('onsuccess', 'successHandler');
              saveToAndroidPay.setAttribute('onfailure', 'failureHandler');

              document.querySelector('#android-pay-btn').appendChild(saveToAndroidPay);
            })
        ).done(function () {
            $('#download-android-note-2').toggle();
            //$('#android-btn').click();
            $(window).scrollTop(0);

            // It will execute after all above ajax requests are successful.
            script = document.createElement('script');
            script.src = 'https://apis.google.com/js/plusone.js';
            document.head.appendChild(script);
        });
    }

    function showShareDialog() {
        $(window).scrollTop(0);
        $('.overlay, #share').toggle();
    }

    //Show Share
    if (localStorage.getItem('bodyshop_b_customer_id') !== null) {
        showShareDialog();
    } else {
        //show note
        if (localStorage.getItem('bodyshop_b_visit_id') !== null) {
            if (getMobileOperatingSystem() == 'iOS') {
                //$('.overlay, #download-ios-note').toggle();
            }

            if (getMobileOperatingSystem() == 'Android') {
                //$('.overlay, #download-android-note').toggle(); 
            }
        }
    }

    //create new record for visitor
    if (localStorage.getItem('bodyshop_b_visit_id') === null) {
        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/bodyshop-b/dashboard/logs"); ?>',
          dataType: 'json',
          data: { 
            visit_id: 0,
            action: 'visitor',
            page: 'landing',
            from_channel: channel
          },
          success : function(r) {
            if (r.visit_id > 0 ) {
                localStorage.setItem('bodyshop_b_visit_id', r.visit_id);

                //show note
                if (getMobileOperatingSystem() == 'iOS') {
                    $('.apple-skip-btn').click();
                    //$('.overlay, #download-ios-note').toggle(); 
                    return;
                }

                if (getMobileOperatingSystem() == 'Android') {
                    $('.android-skip-btn').click();
                    //$('.overlay, #download-android-note').toggle(); 
                    return;
                }

            }
          }
        });   
    }

    $('#android-btn').click(function() {
        $(this).toggle();
        $('#android-pay-btn').toggle();
        return;
    });

    //show form
    $('.apple-note-btn, .apple-skip-btn').click(function(e) {
        e.preventDefault();

        var error_msg = $('#download-ios-note').find('.error-msg')[0];
        var error_ul = $('#download-ios-note').find('.error-msg > ul')[0];
        data = getUrlVars($('#register-form').serialize());
        var terms = data.hasOwnProperty('terms_and_conditions'); //is checked
        var subscribe = data.hasOwnProperty('subscribe_news'); //is checked
        var validate = true;
        var skip = $(this).hasClass('apple-skip-btn');
        $(error_ul).html('') //clear

        //mandatory
        if (! terms) {
            $(error_ul).append('<li>Please agree to the Terms & Conditions</li>');
            validate = false;
        }

        //start if susbscribe
        if (subscribe == true && skip == false) {
          //validation here
          if (data.gender == '') {
            $(error_ul).append('<li>Gender is required.</li>');
            validate = false;
          }
          if (data.age == '') {
            $(error_ul).append('<li>Age is required.</li>');
            validate = false;
          }

          if (data.mobile_number == '') {
            $(error_ul).append('<li>Mobile No. is required.</li>');
            validate = false;
          }
        } //end subscribe news is checked
        
        if (validate == false) {
            $(error_msg).css('display', 'block'); 
            window.scrollTo(0,$('#download-ios-note').height()); //to bottom
            return; //show error message
        }

        //return $('#download-ios-note, #download-ios-note-2').toggle(); //show continue message
        return $('#download-ios-note-2').toggle(); //show continue message
    });

    //register then download
    $('.apple-note-btn-2').click(function() {
        $('.overlay, #download-ios-note-2').toggle();
        return download('#register-form');
    });

    //Download apple voucher
    $('#apple-btn').click(function() {
            var clicked = false;

            if (clicked == true) return; //do nothing

            //create new record
            $.ajax({
              method: "POST",
              url: '<?php echo base_url("client/bodyshop-b/dashboard/logs"); ?>',
              dataType: 'json',
              data: { 
                visit_id: localStorage.getItem('bodyshop_b_customer_id'),
                action: 'download',
                channel: 'ios',
                page: 'landing/ios'
              },
              beforeSend: function() {
                clicked = true;
              },
              success : function(r) {
                clicked = false; //open the function
                  

                if (r.visit_id > 0 ) {

                    //alert('Take note of customer ID for push notification:' + r.visit_id);
                    
                    //$('#frame').prop('src' , '<?php //echo base_url("IosVoucher/createVoucher/4/") ?>' + r.visit_id);

                    window.location = '<?php echo base_url("IosVoucher/createVoucher/4/") ?>' + r.visit_id;

                    setTimeout(function(){ 
                        showShareDialog();
                    }, 2000);

                    return false;
                } else {
                    alert('Voucher already downloaded.');
                }
              }
            });

    });
    //end ios functions

    //START ANDROID
    $('.android-note-btn, .android-skip-btn').click(function(e) {
        e.preventDefault();

        var error_msg = $('#download-android-note').find('.error-msg')[0];
        var error_ul = $('#download-android-note').find('.error-msg > ul')[0];
        data = getUrlVars($('#register-form-android').serialize());
        var terms = data.hasOwnProperty('terms_and_conditions'); //is checked
        var subscribe = data.hasOwnProperty('subscribe_news'); //is checked
        var validate = true;
        var skip = $(this).hasClass('android-skip-btn');
        $(error_ul).html('') //clear

        //mandatory
        if (! terms) {
            $(error_ul).append('<li>Please agree to the Terms & Conditions</li>');
            validate = false;
        }

        //start if susbscribe
        if (subscribe == true && skip == false) {
          //validation here
          if (data.gender == '') {
            $(error_ul).append('<li>Gender is required.</li>');
            validate = false;
          }
          if (data.age == '') {
            $(error_ul).append('<li>Age is required.</li>');
            validate = false;
          }

          if (data.mobile_number == '') {
            $(error_ul).append('<li>Mobile No. is required.</li>');
            validate = false;
          }        
        } //end subscribe news is checked
        
        if (validate == false) {
            $(error_msg).css('display', 'block'); 
            window.scrollTo(0,$('#download-android-note').height()); //to bottom
            return; //show error message
        }

        //$('#download-android-note').toggle(); //show continue message
        return download('#register-form-android');
    });

    //MISC
    $('#close-push-dialog').click(function() {
        $('.overlay, #overlay-3').toggle();
        return;
    });

        
</script>

