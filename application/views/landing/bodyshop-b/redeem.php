<body style="background-color: white !important;">

  <div class="container">
    <div class="row">
      <div class="col-md-12" align="center">
        <img class="img-responsive" src="<?php echo base_url('assets/images/bodyshop/thebodyshop-logo.jpg'); ?>" >
      </div> 
    </div>

    <div class="row">
      <div class="col-md-12" align="center">
        <img class="img-responsive" src="<?php echo base_url('assets/images/bodyshop-b/truthordare.jpg'); ?>" >
      </div> 
    </div>

    <div class="row">
      <div id="register-form" style="display:none;" class="col-md-12">
          <form style="text-align:center;">
            <button id="redeem-btn" style="width: 100%;border: 1px black solid;border-radius: initial;color: black;background-color: initial;font-size: 28px;margin-top: 60px;" class="btn btn_flat" type="submit">Redeem</button>
          </form>
      </div>
    </div>
  </div>

  <div id="redeem-note" style="display:none;padding: 10px;position: absolute;top: 40px;width: 100%;z-index: 3;" class="row-fluid">
        <div style="background-color: #00584b;color:white;padding: 30px;" >
            <div class="row">
              <div class="col-md-12" align="center">
                <img style="width: 100px; margin-bottom: 10px;" class="img-responsive" src="<?php echo base_url('assets/images/bodyshop/thebodyshop-logo.jpg'); ?>" >
              </div> 
            </div>
            <div class="col s12">Please enter your details to redeem your voucher.</div>
            <br />
            <div style="font-size: 10px;display: none;" class="error-msg alert alert-danger">
                <ul style="padding-left: 20px;">
                    <li>Age required</li>
                    <li>Gender required</li>
                    <li>Mobile required</li>
                    <li>Please check the terms and conditions</li>
                </ul>
            </div>
            <form class="register-form" id="register-form-2">
                <select style="border: 1px solid black;color: #797878;width: 100%;margin-bottom: 10px;" name="gender">
                  <option value=''>--Gender--</option>
                  <option value='Male'>Male</option>
                  <option value='female'>Female</option>
                </select>
                <input required style="width: 100%;height: 30px" name="age" type="text" placeholder="Age"/>
                <input style="width: 100%;height: 30px" name="mobile_number" type="text" placeholder="Mobile Number"/>

                <div style="font-size: 11px;">
                  <label><input id="subscribe-news" value = "Yes" name="subscribe_news" type="checkbox"> Yes, sign me up to receive news from The Body Shop</label>
                  <label><input checked id="terms-and-conditions" value = "Yes" name="terms_and_conditions" type="checkbox"> Yes, I have read and agree to the 
                    <a style="color:white;" target ="_blank" href = "<?php echo base_url('landing/page/bodyshop/terms'); ?>" ><u>Terms & Conditions</u></a> of this voucher
                  </label>
                </div>
            </form>
            <div class="button-container">
                <div class="note-btn" class="col s12">
                    <div style="color:white;font-size: 12px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Submit</div>
                </div>
                <br />
                <div class="skip-btn" class="col s12">
                    <div style="color:white;font-size: 9px;text-align: center;width: 100%;border-radius: initial;font-weight: bold;text-decoration: underline;margin-top: -10px;" >Skip</div>
                </div>
            </div>
        </div>
  </div>

  <div class="overlay" style="display:none;position: fixed;height: 100%;width: 100%;background-color: rgba(33, 31, 31, 0.5);top: 0px;z-index: 2;" ></div>


</body>
<script>
  var clicked = false;

  //check if already redeemed
  check();

  function getUrlVars(url) {
      var hash;
      var myJson = {};
      var hashes = url.slice(url.indexOf('?') + 1).split('&');
      for (var i = 0; i < hashes.length; i++) {
          hash = hashes[i].split('=');
          myJson[hash[0]] = hash[1];
      }
      return myJson;
  }


  function check() {
    //check if redeemed
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/bodyshop-b/dashboard/check"); ?>',
          dataType: 'json',
          data: { 
            visit_id: '<?php echo $customer_id; ?>',
          },
          success : function(r) {
            if (r.success > 0 ) {
                $('.overlay, #redeem-note, #register-form').toggle(); //show
            }
          }
        });
  }


  //show form
  $('.note-btn, .skip-btn').click(function(e) {
      e.preventDefault();

      var error_msg = $('#redeem-note').find('.error-msg')[0];
      var error_ul = $('#redeem-note').find('.error-msg > ul')[0];
      data = getUrlVars($('#register-form-2').serialize());
      var terms = data.hasOwnProperty('terms_and_conditions'); //is checked
      var subscribe = data.hasOwnProperty('subscribe_news'); //is checked
      var validate = true;
      var skip = $(this).hasClass('skip-btn');
      $(error_ul).html('') //clear
      $(error_msg).css('display', 'none'); //hide 

      //mandatory
      if (! terms) {
          $(error_ul).append('<li>Please agree to the Terms & Conditions</li>');
          validate = false;
      }

      //start if susbscribe
      if (subscribe == true && skip == false) {
        //validation here
        if (data.gender == '') {
          $(error_ul).append('<li>Gender is required.</li>');
          validate = false;
        }
        if (data.age == '') {
          $(error_ul).append('<li>Age is required.</li>');
          validate = false;
        }

        if (data.mobile_number == '') {
          $(error_ul).append('<li>Mobile No. is required.</li>');
          validate = false;
        }
      } //end subscribe news is checked
      
      if (validate == false) {
          $(error_msg).css('display', 'block'); 
          window.scrollTo(0,$('#redeem-note').height()); //to bottom
          return; //show error message
      }

      if (skip) {
          return $('.overlay, #redeem-note').toggle(); //hide note button
      }

      return update('#register-form-2');
  });


  function update(form) {
        //update record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/bodyshop-b/dashboard/logs"); ?>',
          dataType: 'json',
          data: { 
            visit_id: '<?php echo $customer_id; ?>',
            action: 'update-registrant',
            page: 'redeem',
            data: $(form).serialize()
          },
          success : function(r) {
            if (r.visit_id > 0 ) {
                $('.overlay, #redeem-note').toggle(); //hide note button
                return;
            }
          }
        });
    }

   $('#redeem-btn').click(function(e) {
        e.preventDefault();

        if (clicked == true) return; //do nothing

        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/bodyshop-b/dashboard/redeem"); ?>',
          dataType: 'json',
          data: { 
            visit_id: '<?php echo $customer_id; ?>',
            action: 'redeem',
            page: 'redeem/confirm',
            code: $('#code').val(),
          },
          beforeSend: function() {
            clicked = true;
          },
          success : function(r) {
            clicked = false; //open the function

            if (r.success == true && r.outlet == true) {
                alert('Thank you.');
            } else if(r.outlet == false){
                alert('Wrong outlet code.');
            } else {
                alert('Already redeemed.');
            }
          }
        });
    });
</script>