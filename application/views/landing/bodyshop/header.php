<!DOCTYPE html>
 <html>
 <head>
  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/landing-page.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">

  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>


  <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

     
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="format-detection" content="telephone=no" />
  <script src="<?php echo base_url('assets/js/jquery-2.0.3.min.js'); ?>"></script> 
  <script src="<?php echo base_url('assets/js/materialize/jquery.mobile.custom.js'); ?>"></script>

  <meta property="og:type" content="website" />
  <?php if (! isset($_GET['r'])) { ?>
  <meta property="og:title" content="The Body Shop - Duck N' Dive" />
  <meta property="og:description" content="Get it at just $15 (U.P $22)! Click now." />
  <?php } ?>
  <meta property="og:image" content="<?php echo base_url('assets/images/bodyshop/duckndive2.jpg'); ?>" />

  <?php if (isset($_GET['r'])) { ?>
    <?php if ($_GET['r'] == 'twitter') { ?>
         <meta property="og:title" content="The Body Shop - twitter test" />
         <meta property="og:description" content="Get the Duckndive promo now! Get the Duckndive promo now! Get the Duckndive promo now! Get the Duckndive promo now" />
    <?php } ?>

    <?php if ($_GET['r'] == 'linkedin') { ?>
          <meta property="og:title" content="The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop" />
          <meta property="og:description" content="Get the Duckndive promo now! Get the Duckndive promo now! Get the Duckndive promo now! Get the Duckndive promo now! Get the Duckndive promo now! Get the Duckndive promo now! Get the Duckndive promo now! Get the Duckndive promo now!" />
    <?php } ?>
  <?php } ?>
</head>

<style>
@font-face {
    font-family: 'american_typewriter';
    src: url(<?php echo base_url('assets/images/bodyshop/fonts/American-Typewriter-Regular.woff2'); ?>) format('woff2'),
        url(<?php echo base_url('assets/images/bodyshop/fonts/American-Typewriter-Regular.woff'); ?>) format('woff');
    font-weight: normal;
    font-style: normal;
}

body {
  font-family: 'american_typewriter';
}

</style>