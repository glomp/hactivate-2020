<body style="background-color: white !important;">

  <div class="container">
    <div class="row">
      <div style = "margin-bottom: 10px;" class="col-md-12" align="left">
        <p style="font-size: 12px;">
          <b>THE BODY SHOP DUCK AND DIVE OFFER </b><br /><br />
          TERMS & CONDITIONS <br /><br />
          The information you provide will be used consistent with The Body Shop Singapore’s Privacy Policy (<a href= "https://www.thebodyshop.com/en-sg/termsconditions/services/privacy-policy" >https://www.thebodyshop.com/en-sg/termsconditions/services/privacy-policy</a>).  <br /><br />
          LIMITED TIME OFFER OVERVIEW <br /><br />
          The first 15,000 eligible participants who access and successfully completed The Body Shop Singapore Duck & Dive game will have an opportunity to receive one (1) of 15,000 Duck & Dive coupons. This Offer is available only for a limited time or while supplies last. (See full details below.)  <br /><br />
          ELIGIBLE PARTICIPANTS<br /><br />
           The Offer is only open to senders and recipients who (i) are legal Singapore residents physically residing in Singapore (ii) are 18 years of age or older at time of registration. All federal, state, and local laws and regulations apply.  <br /><br />
          OFFER PERIOD <br /><br />
          This Offer shall run till 11:59 pm SGT on Monday, Dec 25th, 2017 or until the supply of 15,000 Duck & Dive coupon runs out (“Offer Period”).  The Body Shop Singapore will maintain the official time clock and will determine the order of requests received for the Offer.  There is a limit of one (1) Duck and Dive coupon per eligible participant. Any attempt by any person to submit multiple offer requests using multiple/different accounts or any other methods will void that all that person’s offer requests and that person will be disqualified at the The Body Shop Singapore’s discretion.  <br /><br />
          Coupons are non-transferable.  No substitutions, cash-outs, or alternative merchandise will be provided.   <br /><br />
          RELEASE<br /><br />
           By participating in this Offer, participants agree to release, discharge, and hold harmless The Body Shop Singapore and their respective parents, affiliates, subsidiaries, advertising and promotions agencies, and other individuals engaged in the development or execution of this Offer, from any liability, claims, losses, and damages arising out of or relating to their participation in this Offer or the acceptance, use, misuse, or possession of any product received in connection with this Offer.<br /><br />
          The Body Shop Singapore are not responsible for any typographical or other error in the printing of this offer, including erroneous appearance of qualification for the Offered Product and under no circumstances will more than the stated number of products be awarded.  If for any reason The Body Shop Singapore is prevented from continuing with this Offer, or if the integrity and/or feasibility of the Offer is severely undermined by any event including but not limited to fire, flood, epidemic, earthquake, explosion, labor dispute or strike, act of God or public enemy, satellite or equipment failure, riot or civil disturbance, war (declared or undeclared), terrorist threat or activity, or any federal, state, provincial, or local government law, order or regulation, order of any court or jurisdiction, The Body Shop Singapore shall have the right, in its sole discretion, to abbreviate, modify, suspend, cancel or terminate the Offer without further obligation.  <br /><br />
          CAUTION: ANY ATTEMPT BY A PARTICIPANT OR ANY OTHER INDIVIDUAL TO DELIBERATELY ALTER OR DAMAGE ANY WEBSITE, APP, OR SERVICE OR UNDERMINE THE LEGITIMATE OPERATIONS OF THE OFFER ARE PROHIBITED AND MAY BE A VIOLATION OF CRIMINAL AND CIVIL LAWS.  SHOULD SUCH AN ATTEMPT BE MADE, THE BODY SHOP SINGAPORE RESERVE THE RIGHT TO SEEK DAMAGES FROM ANY SUCH PERSON TO THE FULLEST EXTENT PERMITTED BY LAW.<br /><br />
          BY PARTICIPATING AS AN ACTUAL OR PROSPECTIVE OFFERED PRODUCT SENDER OR RECIPIENT YOU AGREE TO BE BOUND BY THESE TERMS AND CONDITIONS, INCLUDING ALL LIMITATIONS OF LIABILITY AND DISCLAIMERS, AS SPECIFIED HEREIN.<br /><br />
          The Body Shop (Singapore) Pte Ltd, 3 Killiney Rd #06-02/06, Winsland House 1, Singapore 23951
        </p>

        <button class="btn btn-default" id ="close">Close</button>
      </div> 
    </div>

  </div>

</body>

<script type="text/javascript">
    $('#close').click(function(e) {
       window.close();
    });
</script>