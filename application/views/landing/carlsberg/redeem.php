<style type="text/css">
  .btn {
    font-size: 10px !important;
  }
</style>
<body style="background-color: #00584b !important;color:white;">

  <div class="container">

    <div class="row" style="margin-top:  5px;">
      <div class="col-md-12" align="center">
        <img style="width: 100%" class="img-responsive" src="<?php echo base_url('assets/images/carlsberg/outlets/cayote-logo.jpg'); ?>" >
      </div> 
    </div>

    <div class="row" style="margin-top:  5px;">
      <div style="padding-right: 2px" class="col-xs-6" align="center">
        <button data-code="1" style="width: 100%" class="btn btn-default btn-square redeem">Discovery Bay</button>
      </div> 
      <div style=" padding-left: 2px" class="col-xs-6" align="center">
        <button data-code="2" style="width: 100%" class="btn btn-default btn-square redeem">Wanchai</button>
      </div>
    </div>

    <div class="row" style="margin-top:  5px;">
      <div class="col-md-12" align="center">
        <img style="width: 100%" class="img-responsive" src="<?php echo base_url('assets/images/carlsberg/outlets/mcsorleys-logo.jpg'); ?>" >
      </div> 
    </div>

    <div class="row" style="margin-top:  5px;">
      <div style="padding-right: 2px" class="col-xs-6" align="center">
        <button data-code="3" style="width: 100%" class="btn btn-default btn-square redeem">Soho</button>
      </div> 
      <div style="padding-left: 2px" class="col-xs-6" align="center">
        <button data-code="4" style="width: 100%" class="btn btn-default btn-square redeem">Discovery Bay</button>
      </div> 
    </div>

    <div class="row" style="margin-top:  5px;">
      <div class="col-md-12" align="center">
        <img style="width: 100%" class="img-responsive" src="<?php echo base_url('assets/images/carlsberg/outlets/grandcentral-logo.jpg'); ?>" >
      </div> 
    </div>

    <div class="row" style="margin-top:  5px;">
      <div class="col-md-12" align="center">
        <button data-code="5" style="width: 100%" class="btn btn-default btn-square redeem">Tsim Sha Tsui</button>
      </div> 
    </div>

    <div class="row" style="margin-top:  5px;">
      <div class="col-md-12" align="center">
        <img style="width: 100%" class="img-responsive" src="<?php echo base_url('assets/images/carlsberg/outlets/thejockey-logo.jpg'); ?>" >
      </div> 
    </div>

    <div class="row" style="margin-top:  5px;">
      <div class="col-md-12" align="center">
        <button data-code="6" style="width: 100%" class="btn btn-default btn-square redeem">Happy Valley</button>
      </div> 
    </div>

    <div class="row" style="margin-top:  5px;">
      <div class="col-md-12" align="center">
        <img style="width: 100%" class="img-responsive" src="<?php echo base_url('assets/images/carlsberg/outlets/wildfire-logo.jpg'); ?>" >
      </div> 
    </div>

    <div class="row" style="margin-top:  5px; margin-bottom: 10px;"">
      <div style="padding-right: 2px" class="col-xs-4" align="center">
        <button data-code="7" style="width: 100%;height: 28px;font-size: 9px !important" class="btn btn-default btn-square redeem">Knutsford Terrace</button>
      </div> 

      <div style="padding-right: 2px;padding-left: 2px;" class="col-xs-4" align="center">
        <button data-code="9" style="width: 100%" class="btn btn-default btn-square redeem">The Peak</button>
      </div> 
      <div style="padding-left: 2px" class="col-xs-4" align="center">
        <button data-code="10" style="width: 100%" class="btn btn-default btn-square redeem">Fashion Walk</button>
      </div> 
    </div>
    
  </div>

</body>
<script>
  var clicked = false;

   $('.redeem').click(function(e) {
        e.preventDefault();

        var r = confirm("You are about to redeem an item.");
        if (r == false) {
            return;
        }

        if (clicked == true) return; //do nothing

        var code = $(this).data('code')

        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/carlsberg/dashboard/redeem"); ?>',
          dataType: 'json',
          data: { 
            visit_id: '<?php echo $customer_id; ?>',
            action: 'redeem',
            page: 'redeem/confirm',
            code: code          
          },
          beforeSend: function() {
            clicked = true;
          },
          success : function(r) {
            clicked = false; //open the function

            if (r.success == true && r.outlet == true) {
                alert('Thank you.');
            } else if(r.outlet == false){
                alert('Wrong outlet code.');
            } else {
                alert('Already redeemed.');
            }
          }
        });
    });
</script>