<div class="main-container">
    <img class="responsive-img" src="<?php echo base_url('assets/images/hactivate-logo.png'); ?>" >
    <p class="form-description">
        Welcome to h.@ctiv8. A true O2O mobile solution featuring transparent tracking, push and geolocation reminders.
    </p>
    <p>Please  enter "123" below as the outlet ID code and submit</p>
    <br />
    <form class="register-form" style="text-align:center">
        <input id="code" type="text" placeholder="Outlet Code"/><br />
        <button id="redeem-btn" style="border: 1px white solid;border-radius: initial;color: white;background-color: initial;" class="btn btn_flat" type="submit">Submit</button>
    </form>
</div>

<script>
  var clicked = false;

   $('#redeem-btn').click(function(e) {
        e.preventDefault();

        if (clicked == true) return; //do nothing

        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/demo/dashboard/redeem"); ?>',
          dataType: 'json',
          data: { 
            visit_id: '<?php echo $customer_id; ?>',
            action: 'redeem',
            page: 'redeem/confirm',
            code: $('#code').val(),
          },
          beforeSend: function() {
            clicked = true;
          },
          success : function(r) {
            clicked = false; //open the function

            if (r.success == true && r.outlet == true) {
                alert('Thank you.');
            } else if(r.outlet == false){
                alert('Wrong outlet code.');
            } else {
                alert('Already redeemed.');
            }
          }
        });
    });
</script>