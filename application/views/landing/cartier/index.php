<?php 
  function current_full_url()
  {
      $CI =& get_instance();

      $url = $CI->config->base_url($CI->uri->uri_string());
      return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
  }

?>

<style type="text/css">
    .map-link a, a:active, a:link, a:visited, a:hover{ 
        color: white;
        text-decoration: none;
     }
</style>
  <body style="background: #bd0914 !important">
    <div class="overlay" style="display:none;position: fixed;height: 100%;width: 100%;background-color: rgba(33, 31, 31, 0.84);top: 0px;z-index: 2;"><i class="fa fa-close" id="x-overlay" style="color: white; float: right; padding: 5px;"></i></div>

    <div id="download-note" style="margin-top: 10px;" class="row-fluid">
        <div style="color:white;padding: 30px; padding-top: 10px;" >
            <div class="row">
              <!-- <div class="col-md-6 col-sm-6 col-xs-6" align="right">
                <img style="width: 70%;" class="img-responsive" src="<?php echo base_url('assets/images/cartier/logo.png'); ?>" >
              </div>  -->
              <div class="col-md-12 col-sm-12 col-xs-12" align="center">
                <img style="width: 70%; padding-top: 5px;" class="img-responsive" src="<?php echo base_url('assets/images/cartier/prince2.png'); ?>" >
              </div> 
            </div>
            <div style="margin-top: 30px;" class="row">
              <div style = "padding: 2px;" class="col-xs-12" align="center">
                太子珠寶鐘錶為您呈獻卡地亞專屬禮遇
                <br /><br />
              </div>
            </div>
            <div class="row">
              <div id="iphone-display" style="padding: 2px;" class="col-xs-12" align="center">
                <div style="border: 1px solid; width: 70%;" class="get-certificate">
                    <i class="fa fa-download" style="font-size: 20px; padding: 5px 0;"></i>
                    &nbsp;&nbsp;&nbsp;&nbsp;下載禮遇專函
                </div>
                <br />
              </div>
            </div>

            <div class="row">
              <div style = "padding: 2px; font-size: 9pt;" class="col-xs-12">
                
                誠邀您蒞臨以下太子珠寶鐘錶專門店，探索卡地亞作品系列。<br /> 
                與此同時，於2018年10月12日或之前選購卡地亞腕錶並出示<br />
                此禮遇專函，即可獲享卡地亞專屬禮遇乙份<br />
                （數量有限，送完即止）。<br />
              </div>
            </div>
            <div class="row" style="margin-top: 40px;" >
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="row">
                      <div style = "padding: 2px; font-size: 9pt;" class="col-xs-12">
                        <a class="map-link" target="_blank" href= "http://bit.do/exaxx" >
                        彌敦道總店<br /> 
                        九龍尖沙咀彌敦道23-25號<br />
                        彩星中心地下及地庫<br />
                        </a>
                      </div>
                    </div>
                    <div class="row">
                      <div style = "padding: 2px; padding-top: 13px; font-size: 9pt;" class="col-xs-12">
                        <a class="map-link" target="_blank" href= "http://bit.do/exaxP" >
                        太子集團中心分店<br /> 
                        九龍尖沙咀北京道12號A地<br />
                        下及1樓<br />
                        </a>
                      </div>
                    </div>
                    <div class="row">
                      <div style = "padding: 2px; padding-top: 13px; font-size: 9pt;" class="col-xs-12">
                        <a class="map-link" target="_blank" href= "http://bit.do/exaxW" >
                        海洋中心卡地亞專門店<br /> 
                        九龍尖沙咀海港城海洋中心<br />
                        3樓319號舖<br />
                        </a>
                      </div>
                    </div>
                    <div class="row">
                      <div style = "padding: 2px; padding-top: 13px; font-size: 9pt;" class="col-xs-12">
                        <a class="map-link" target="_blank" href= "http://bit.do/exayx" >
                        羅素街分店<br /> 
                        銅鑼灣羅素街58號鋪地下<br />
                        至3樓B-C號鋪<br />
                        </a>
                      </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="row">
                      <div class="col-md-12" align="center">
                        <img style="width: 60%;" class="img-responsive" src="<?php echo base_url('assets/images/cartier/logo2.png'); ?>" >
                      </div> 
                    </div>
                    <div style="margin-top: 20px;" class="row">
                      <div class="col-md-12" align="center">
                        <img style="width: 80%" class="img-responsive" src="<?php echo base_url('assets/images/cartier/red-watch.jpg'); ?>" >
                      </div> 
                    </div>
                    <div style="margin-top: 15px;" class="row">
                      <div class="col-md-12" align="center">
                        <img style="width: 40%" class="img-responsive" src="<?php echo base_url('assets/images/cartier/prince2.png'); ?>" >
                      </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="desktop-note" style="display:none;padding: 10px;position: absolute;top: 60px;width: 100%;z-index: 3" class="row-fluid">
        <div style="color:white; padding: 50px;" >
            <div class="col s12">In order to download the voucher, please go to http://[change landing page url] on your mobile.</div>
        </div>
    </div>
    <div id="android-note" style="display:none;padding: 10px;position: absolute;top: 60px;width: 100%;z-index: 3" class="row-fluid">
        <div style="color:white; padding: 50px;" >
            <div class="col s12">Android 或其他用戶只需出示專函螢幕截圖便可享用此優惠。</div>
        </div>
    </div>

    <div id="fb-note" style="display:none;padding: 10px;position: absolute;width: 100%;z-index: 3" class="row-fluid">
        <div style="color:white; padding: 20px;padding-top: 0px" >

            <div class="row">
              <div class="col-md-12" align="center">
                <img style="width: 100%" class="img-responsive" src="<?php echo base_url('assets/images/cartier/logo.png'); ?>" >
              </div> 
            </div>

            <div class="row">
                <div class="col col-xs-12" align="justify">
                Thank you for choosing to enjoy this promotion. The promotion's website can only be viewed in your default browser. Please click the button below to copy the voucher's website. Then in your default browser such as Safari or Chrome, please paste into your browser's address bar.
                <br /><br />
                Thank you.
                <br /><br />
                多謝你享用此優惠。此優惠網頁 只會顯示於你的預設瀏覽器。請 按下複製，然後把網址貼上至你 的預設瀏覽器。
                <br />
                <br />
                謝謝!
                <input id="fb-url" style="color: black" type="hidden" value="<?php echo base_url(uri_string()); ?>" />
                <br /><br />
                <div id="fb-btn" data-clipboard-text="<?php echo current_full_url(); ?>" style="color:white;font-size: 12px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat fb-btn">Copy</div>
              </div>
            </div>
        </div>
    </div>



  </body>


<script>
    var base_url = '<?php echo base_url(); ?>';
    var clipboard = new Clipboard('#fb-btn');

    clipboard.on('success', function(e) {
        $('#fb-btn').text('Copied!');
    });

    function isFacebookApp() {
        var ua = navigator.userAgent || navigator.vendor || window.opera;

        return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1);
    }

    if ( isFacebookApp()) {
        $('.overlay, #fb-note').toggle();
        throw new Error("Something went badly wrong!"); //just to finish execution
    }


    function getUrlVars(url) {
        var hash;
        var myJson = {};
        var hashes = url.slice(url.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            myJson[hash[0]] = hash[1];
        }
        return myJson;
    }

    function getMobileOperatingSystem() {
      var userAgent = navigator.userAgent || navigator.vendor || window.opera;

          // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            return "iOS";
        }

        return "unknown";
    }


    if (getMobileOperatingSystem() == 'unknown') {
        $('#iphone-display').toggle();
        $('.overlay, #desktop-note, #x-overlay').toggle(); //desktop note
        throw new Error("Something went badly wrong!"); //just to finish execution
    } else if (getMobileOperatingSystem() == 'Android') {
        //$('#device-choose').text('Android');
        //$('.overlay, #android-note').toggle(); //desktop note
    }

    $('.get-certificate').click(function() {
        //live
        window.location = base_url + '/landing/page/cartier/get';
        //local
        //window.location = 'http://localhost/hactivate/landing/page/cartier/get';
        return;
    }); 

    $('#x-overlay').click(function() {
        $('.overlay, #android-note').toggle(); //desktop note
    }); 
</script>

