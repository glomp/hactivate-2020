<?php $this->load->view('client/demo/header'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. of Page visitor</span>
              <div class="count green"><?php echo $total_page_visitor; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/demo/reports/page_visitor'); ?>">View</a></span>
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. of Registrant</span>
              <div class="count green"><?php echo $total_register; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/demo/reports/registrants'); ?>">View</a></span>
            </div>      

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. of IOS Downloads</span>
              <div class="count green"><?php echo $total_ios_downloads; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/demo/reports/download_from_ios'); ?>">View</a></span>
            </div>      


            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. of Androids Downloads</span>
              <div class="count green"><?php echo $total_android_downloads; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/demo/reports/download_from_android'); ?>">View</a></span>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. of IOS Redemptions</span>
              <div class="count green"><?php echo $total_ios_redemption; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/demo/reports/redemptions/ios'); ?>">View</a></span>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. of Android Redemptions</span>
              <div class="count green"><?php echo $total_android_redemption; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/demo/reports/redemptions/android'); ?>">View</a></span>
            </div>

          </div>
        </div>
        <!-- /top tiles -->

          </div>
  
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            h.@ctiv8 <?php echo date('Y'); ?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url('assets/admin/vendors/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url('assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url('assets/admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url('assets/admin/vendors/DateJS/build/date.js'); ?>"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url('assets/admin/vendors/moment/min/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url('assets/admin/build/js/custom.min.js'); ?>"></script>

  </body>
</html>
