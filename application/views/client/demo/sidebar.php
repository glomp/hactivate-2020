<!-- sidebar menu -->
  <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
      <h3>General</h3>
      <ul class="nav side-menu">
        <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="<?php echo base_url('client/demo/dashboard'); ?>"><i class="fa fa-desktop"></i>Dashboard</a></li>
            <li class="hidden">
              <a href="<?php echo base_url('client/demo/reports/graphs'); ?>"><i class="fa fa-line-chart"></i>Graphs</a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
<!-- /sidebar menu -->