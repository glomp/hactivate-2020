<?php $this->load->view('client/bodyshop/header'); ?>

<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
       <a id = "export-btn" href="<?php echo base_url('client/bodyshop/reports/export_registrants'); ?>" class="btn btn-primary btn-md">Export to Spreadsheet</a>  
     </div>
     <br />

     <div class="row">
		<table id="registrants" class="table" width="100%">
			<thead>
				<tr>
					<th>Customer ID</th>
					<th>Info</th>
					<th>Date Time</th>
				</tr>
			</thead>
		</table>

	 </div>

</div>
<!-- /page content -->

<div class="overlay" style="display: none; position: fixed;height: 100%;width: 100%;background-color: rgba(192, 192, 192, 0.7);top: 0px;z-index: 2" ></div>

<div id="overlay-2" style="display:none;padding: 10px;position: absolute;top: 123px;width: 100%;z-index: 3" class="row-fluid">
	<form id ="send_message" action="<?php echo base_url('IosVoucher/message'); ?>" method="POST">
		<input id ="customer_id" name="customer_id" type="hidden" />
		<input id ="campaign_id" name="campaign_id" type="hidden" value="9999" />
	    <div style="background-color: #505153;padding: 50px;" class="col s12">
	        <div  class="col s12 ">
	        	<label style="color:white">Message</label>
	            <textarea id="message" style="width: 100%" name="message"></textarea>
	        </div>
	        <div id="submit-btn" style="margin-top: 22px;" class="col s12">
	            <div style="color:white;font-size: 18px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Submit</div>
	        </div>

	        <div id="cancel-btn" style="margin-top: 22px;" class="col s12">
	            <div style="color:white;font-size: 18px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Cancel</div>
	        </div>
	    </div>
    </form>
</div>


	<!-- footer content -->
<footer>
  <div class="pull-right">
    h.@ctiv8 <?php echo date('Y'); ?>
  </div>
  <div class="clearfix"></div>
</footer>
<!-- /footer content -->

</div>
</div>

<!-- jQuery -->
<script src="<?php echo base_url('assets/admin/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url('assets/bower_components/datatables.net/js/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/vendors/moment/min/moment.min.js'); ?>"></script>

<!-- Custom Theme Scripts -->
<script src="<?php echo base_url('assets/admin/build/js/custom.min.js'); ?>"></script>

<!-- Bootstrap data tables -->
<link href="<?php echo base_url('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">

<!-- Flot -->
<script>
	$(document).ready(function() {

		$('body').on('click', '.message', function(e){
			e.preventDefault();

			var uid = $(this).data().uid;
			$('#customer_id').val(uid);
			$('.overlay, #overlay-2').toggle();
		});

		$('#submit-btn').click(function(e){
			e.preventDefault();

			$.ajax({
              method: "POST",
              url: $('#send_message').attr('action'),
              dataType: 'json',
              data: { 
                message: $('#message').val(),
                campaign_id: $('#campaign_id').val(),
                client_ids: $('#customer_id').val(),
                broadcast: false
              },
              success : function(r) {
              	$('#message').val('');
              	alert('Message sent.');
              }
            });
		});

		$('#cancel-btn').click(function(e){
			e.preventDefault();
			$('.overlay, #overlay-2').toggle();
		});

		 $('#registrants').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "paging": true,
		    "searching": false,
		    "order": [[ 2, 'desc' ]],
		    "ajax": {
		        "url": window.location,
		        "type": "POST"
		    },
		    "columnDefs": [ {
			    "targets": 2,
			    "render": function ( data, type, row ) {
			    	return moment(data).format('MMMM D, YYYY, h:mm:ss A');
		            //return data +' ('+ row[3]+')';
		        },
		     //    "targets": 3,
			    // "render": function ( data, type, row ) {
			    // 	return '<a data-uid="'+ row.operations +'" class="btn btn-default message" href="#" >Message</a>';
		     //        //return data +' ('+ row[3]+')';
		     //    },
		  	} ],
		    "columns": [
		        { "data": "uid", "orderable": false },
		        { "data": "info", "orderable": false },
		        { "data": "date_created" },
		        // { "data": "operations", "orderable": false }
		    ]
		} );
	});
</script>