<?php $this->load->view('client/cartier/header'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row ">
            <div class="col-md-12 ">
              <span class="count_top"><i class="fa fa-user"></i> No. of Page Vistor vs. Site Entrants</span>
            </div>

            <div class="col-md-12 ">
              <canvas id="lineChart"></canvas>
            </div>
          </div>

          <div class="row ">
            <div class="col-md-12 ">
              <span class="count_top"><i class="fa fa-user"></i> No. of Site Entrants vs. IOS Downloads vs Android Downloads</span>
            </div>

            <div class="col-md-12 ">
              <canvas id="lineChart2"></canvas>
            </div>
            
             
          </div>

          <div class="row">
            <div class="col-md-12 ">
              <span class="count_top"><i class="fa fa-user"></i> Redemptions</span>
            </div>

            <div class="col-md-12 ">
              <canvas id="lineChart4"></canvas>
            </div>
          </div>
          <!-- /top tiles -->

          </div>
  
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            H@ctivate <?php echo date('Y'); ?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url('assets/admin/vendors/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url('assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url('assets/admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url('assets/admin/vendors/DateJS/build/date.js'); ?>"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url('assets/admin/vendors/moment/min/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

    <!-- Chart.js -->
    <script src="<?php echo base_url('assets/admin/vendors/Chart.js/dist/Chart.min.js') ?>"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url('assets/admin/build/js/custom.min.js'); ?>"></script>

    <script>
        var graphs = <?php echo $graphs; ?>;

        var ctx = document.getElementById("lineChart");
        var lineChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: graphs.labels,
          datasets: [{
            label: "Page Visits",
            backgroundColor: "rgba(38, 185, 154, 0.31)",
            borderColor: "rgba(38, 185, 154, 0.7)",
            pointBorderColor: "rgba(38, 185, 154, 0.7)",
            pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointBorderWidth: 1,
            data: graphs.visitors,
            }, {
            label: "Site Entrants",
            backgroundColor: "rgba(3, 88, 106, 0.3)",
            borderColor: "rgba(3, 88, 106, 0.70)",
            pointBorderColor: "rgba(3, 88, 106, 0.70)",
            pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(151,187,205,1)",
            pointBorderWidth: 1,
            data: graphs.registrants
          }]
        },
        options: {
                tooltips: {
                  callbacks: {
                      label: function(tooltips, data) {
                        return data.datasets[tooltips.datasetIndex].label + ' : ' + tooltips.yLabel;
                    }
                  }
                },
            }
        });


        var ctx2 = document.getElementById("lineChart2");
        var lineChart2 = new Chart(ctx2, {
        type: 'line',
        data: {
          labels: graphs.labels,
          datasets: [{
            label: "Site Entrants",
            backgroundColor: "rgba(38, 185, 154, 0.31)",
            borderColor: "rgba(38, 185, 154, 0.7)",
            pointBorderColor: "rgba(38, 185, 154, 0.7)",
            pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointBorderWidth: 1,
            data: graphs.registrants,
            }, {
            label: "IOS Downloads",
            backgroundColor: "rgba(3, 88, 106, 0.3)",
            borderColor: "rgba(3, 88, 106, 0.70)",
            pointBorderColor: "rgba(3, 88, 106, 0.70)",
            pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(151,187,205,1)",
            pointBorderWidth: 1,
            data: graphs.ios
            },{
            label: "Android Downloads",
            backgroundColor: "rgba(92, 173, 5, 0.3)",
            borderColor: "rgba(124, 148, 5, 0.67)",
            pointBorderColor: "rgba(124, 148, 5, 0.67)",
            pointBackgroundColor: "rgba(124, 148, 5, 0.67)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(151,187,205,1)",
            pointBorderWidth: 1,
            data: graphs.android
          }]
        },
        options: {
                tooltips: {
                  callbacks: {
                      label: function(tooltips, data) {
                        return data.datasets[tooltips.datasetIndex].label + ' : ' + tooltips.yLabel;
                    }
                  }
                },
            }
        });
        
        var ctx4 = document.getElementById("lineChart4");
        var lineChart4 = new Chart(ctx4, {
        type: 'bar',
        scaleOverride : true,
        scaleSteps : 10,
        scaleStepWidth : 50,
        scaleStartValue : 0,
        data: {
          labels: ['Nathan Road Head Office', 'Prince Edward Group Center Branch', 'Ocean Center', 'Russell Street Branch'],
          datasets: [{
            label: "iOS",
            backgroundColor: "rgba(3, 88, 106, 0.3)",
            borderColor: "rgba(3, 88, 106, 0.70)",
            pointBorderColor: "rgba(3, 88, 106, 0.70)",
            pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(151,187,205,1)",
            pointBorderWidth: 1,
            data: graphs.redemptions.ios,
          },
          {
            label: "Android",
            backgroundColor: "rgba(92, 173, 5, 0.3)",
            borderColor: "rgba(124, 148, 5, 0.67)",
            pointBorderColor: "rgba(124, 148, 5, 0.67)",
            pointBackgroundColor: "rgba(124, 148, 5, 0.67)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(151,187,205,1)",
            data: graphs.redemptions.android
          }]
        },
        options: {
                scales: {
                    yAxes : [{
                        ticks: {
                            min: 0,
                            stepSize: 5,
                        }
                    }],
                    xAxes: [{
                        barPercentage: 0.8
                    }]
                }

            }
        
        });

    </script>

  </body>
</html>
