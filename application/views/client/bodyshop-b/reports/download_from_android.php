<?php $this->load->view('client/bodyshop-b/header'); ?>

<!-- page content -->
<div class="right_col" role="main">

	<div class="row">
       <a id = "export-btn " href="<?php echo site_url(''); ?>" class="hide btn btn-primary btn-md">Export to Spreadsheet</a>  
     </div>

     <div class="row">
		<table id="downloads-from-android" class="table" width="100%">
			<thead>
				<tr>
					<th>ID</th>
					<th>Date Time</th>
				</tr>
			</thead>
		</table>

	 </div>

</div>
<!-- /page content -->

	<!-- footer content -->
<footer>
  <div class="pull-right">
    h.@ctiv8 <?php echo date('Y'); ?>
  </div>
  <div class="clearfix"></div>
</footer>
<!-- /footer content -->

</div>
</div>

<!-- jQuery -->
<script src="<?php echo base_url('assets/admin/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url('assets/bower_components/datatables.net/js/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/vendors/moment/min/moment.min.js'); ?>"></script>

<!-- Custom Theme Scripts -->
<script src="<?php echo base_url('assets/admin/build/js/custom.min.js'); ?>"></script>

<!-- Bootstrap data tables -->
<link href="<?php echo base_url('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">

<!-- Flot -->
<script>
	$(document).ready(function() {
		 $('#downloads-from-android').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "paging": true,
		    "searching": false,
		    "order": [[ 1, 'desc' ]],
		    "ajax": {
		        "url": window.location,
		        "type": "POST"
		    },
		    "columnDefs": [ {
			    "targets": 1,
			    "render": function ( data, type, row ) {
			    	return moment(data).format('MMMM D, YYYY, h:mm:ss A');
		        },
		  	} ],
		    "columns": [
		        { "data": "id", "orderable": false },
		        { "data": "date_created" }
		    ]
		} );

		$('#export-btn').click(function(e) {
			e.preventDefault();
			alert('Not yet available');
		});
	});
</script>