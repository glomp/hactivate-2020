<!-- sidebar menu -->
  <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
      <h3>General</h3>
      <ul class="nav side-menu">
        <li><a href="<?php echo base_url('client/uemura/dashboard'); ?>"><i class="fa fa-desktop"></i>Dashboard</a></li>
        <li>
          <a href="<?php echo base_url('client/uemura/reports/graphs'); ?>"><i class="fa fa-line-chart"></i>Graphs</a>
        </li>
      </ul>
    </div>
  </div>
<!-- /sidebar menu -->