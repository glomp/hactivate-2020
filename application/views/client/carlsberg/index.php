<?php $this->load->view('client/carlsberg/header'); ?>

        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">
              <div class="col-md-12">
                    <label id="toggle-calendar">Toggle Calendar</label> / <label id="reset-all">Reset</label>
                    <div style="display:none;" class="week-picker"></div>
              </div>
          </div>
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. of Page visitor</span>
              <div class="count green"><?php echo $total_page_visitor; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/carlsberg/reports/page_visitor'); ?>">View</a></span>
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. of Site Entrants</span>
              <div class="count green"><?php echo $total_register; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/carlsberg/reports/registrants'); ?>">View</a></span>
            </div>      

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. of IOS Downloads</span>
              <div class="count green"><?php echo $total_ios_downloads; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/carlsberg/reports/download_from_ios'); ?>">View</a></span>
            </div>      


            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. of Androids Downloads</span>
              <div class="count green"><?php echo $total_android_downloads; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/carlsberg/reports/download_from_android'); ?>">View</a></span>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. of IOS Redemptions</span>
              <div class="count green"><?php echo $total_ios_redemption; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/carlsberg/reports/redemptions/ios'); ?>">View</a></span>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. of Android Redemptions</span>
              <div class="count green"><?php echo $total_android_redemption; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/carlsberg/reports/redemptions/android'); ?>">View</a></span>
            </div>

          </div>
        </div>
        <!-- /top tiles -->

          </div>
  
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            h.@ctiv8 <?php echo date('Y'); ?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <script>

          var siteUrl = '<?php echo base_url() ?>';
          var startDate;
          var endDate;

          $('#toggle-calendar').click(function() {
              $('.week-picker').toggle();
          });

          $('#reset-all').click(function() {
              window.location = siteUrl + 'client/carlsberg/dashboard';
          });
      
                    
          var selectCurrentWeek = function() {
              window.setTimeout(function () {
                  $('.week-picker').find('.ui-datepicker-current-day a').addClass('ui-state-active')
              }, 1);
          }
          
          $('.week-picker').datepicker( {
              showOtherMonths: true,
              selectOtherMonths: true,
              firstDay: 1,
              onSelect: function(dateText, inst) { 
                  var date = $(this).datepicker('getDate');
                  console.log(date.getDay());
                  startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 1);
                  endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 7);
                  var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
                  startDate_t = $.datepicker.formatDate( dateFormat, startDate, inst.settings );
                  endDate_t = $.datepicker.formatDate( dateFormat, endDate, inst.settings );


                  $('#startDate').text($.datepicker.formatDate( dateFormat, startDate, inst.settings ));
                  $('#endDate').text($.datepicker.formatDate( dateFormat, endDate, inst.settings ));

                  window.location = siteUrl + 'client/carlsberg/dashboard?week=' + startDate_t + '-' + endDate_t;
                  
                  selectCurrentWeek();
              },
              beforeShowDay: function(date) {
                  var cssClass = '';
                  if(date >= startDate && date <= endDate)
                      cssClass = 'ui-datepicker-current-day';
                  return [true, cssClass];
              },
              onChangeMonthYear: function(year, month, inst) {
                  selectCurrentWeek();
              }
          });
          
          $('.week-picker .ui-datepicker-calendar tr').on('mousemove', function() { $(this).find('td a').addClass('ui-state-hover'); });
          $('.week-picker .ui-datepicker-calendar tr').on('mouseleave', function() { $(this).find('td a').removeClass('ui-state-hover'); });

    </script>

    <!-- jQuery 
        <script src="<?php //echo base_url('assets/admin/vendors/jquery/dist/jquery.min.js'); ?>"></script> -->
    <!-- Bootstrap -->
    <script src="<?php echo base_url('assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url('assets/admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url('assets/admin/vendors/DateJS/build/date.js'); ?>"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url('assets/admin/vendors/moment/min/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url('assets/admin/build/js/custom.min.js'); ?>"></script>

  </body>
</html>
